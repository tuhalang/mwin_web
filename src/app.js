import React, {Component, Suspense, useEffect} from "react";
import Loading from "./components/Loading";
import {HashRouter, Route, Switch} from "react-router-dom";
import HistoryPage from "./containers/HistoryContainer";
import InfoPage from "./containers/InfoContainer";
import ResultPage from "./containers/ResultContainer";
import Home from "./containers/CurrentAuctionSessionContainer";
import {Redirect} from "react-router";
import actions from "./redux/actions/info";
import {connect} from "react-redux";
import ModalLogin from "./components/Modal/ModalLogin";
import {createBrowserHistory} from "history";
import {useTranslation} from "react-i18next";
import utils from "./common/utils";
import styles from 'assets/jss'
import {makeStyles} from "@material-ui/core/styles";
import Navigation from "./components/Navigation/Navigation";

const hist = createBrowserHistory();
const useStyles = makeStyles(styles);

class ProtectedRoute extends Component {
    render() {
        const {component: Component, authenticated, openLogin, ...props} = this.props

        return (
            <Route
                {...props}
                render={props => {

                    if (authenticated) {
                        return <Component {...props} />
                    } else {
                        openLogin()
                        // hist.replace('/')
                        return <div style={{display: 'flex', justifyContent: 'center'}}><Loading/></div>
                    }
                }}
            />
        )
    }
};

function App(props) {
    const classes = useStyles();
    const {t, i18n} = useTranslation('translation');
    console.log(hist, "hist", hist.location.pathname.lastIndexOf('/') + 1)


    useEffect(() => {
        if (props.isLogout) {
            console.log('isLogout')
            hist.push('/play')
            utils.showNotification(t('logout-modal-notification'), t('logout-notification-success'))
            props.cancelLogout()
        }
    }, [props.isLogout])

    return (
        <div className={classes.container}>
            <div className={classes.container2}>
                <HashRouter history={hist}>
                    <Suspense fallback={<Loading/>}>
                        <Switch>
                            <ProtectedRoute path={"/history"} exact
                                            authenticated={localStorage.getItem('token') ? true : false}
                                            component={HistoryPage} openLogin={props.openLogin}/>
                            <Route path={"/info"} exact component={InfoPage}/>
                            <Route path={"/result"} exact component={ResultPage}/>
                            <Route path={"/play"} exact component={Home}/>
                            <Redirect from="/" to={"/play"}/>
                            {/*<Redirect from="/play" to={"/play"}/>*/}
                        </Switch>
                        <Navigation active={hist.location.hash.substring(2)}
                                    style={{position: 'fixed', bottom: 0}} key={'navigation'}/>
                    </Suspense>
                </HashRouter>
                {props.openModalLogin ? <ModalLogin visible={props.openModalLogin} handleOk={() => {
                    hist.push('/play')
                }} handleCancel={() => {
                    props.cancelModalLogin()
                }} {...props}/> : <div/>}
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    openModalLogin: state.info.openModalLogin,
    isLogout: state.info.isLogout
});

const mapDispatchToProps = (dispatch) => ({
    login: (params, callback) => {
        dispatch(actions.login(params, callback))
    },
    openLogin: () => {
        dispatch(actions.openModalLogin())
    },
    cancelModalLogin: () => {
        dispatch(actions.cancelModalLogin())
    },
    sendOtp: (params, callback) => {
        dispatch(actions.getOtp(params, callback))
    },
    cancelLogout: () => {
        dispatch(actions.logoutSucceed())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
