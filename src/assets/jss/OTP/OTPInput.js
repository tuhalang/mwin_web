import {PRIMARY_COLOR} from "../../../consts";

const OTPInputStyle = {
    inputNumber: {
        width: 40,
        height: 40,
        marginLeft: 4,
        marginRight: 4,
        textAlign: 'center',
        color: PRIMARY_COLOR,
        borderColor: PRIMARY_COLOR,
        '&:hover': {
            borderColor: PRIMARY_COLOR,
            color: PRIMARY_COLOR,
        },
        '&:focus': {
            borderColor: PRIMARY_COLOR,
            color: PRIMARY_COLOR,
        }
    }
}

export default OTPInputStyle;
