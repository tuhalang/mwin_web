import {PRIMARY_COLOR} from "../../../consts";
// import {isMobile} from 'react-device-detect';
import {isSafari} from 'react-device-detect';

const auctionStyle = {
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flex: 1,
        backgroundColor: 'white',
        padding: 16,
        marginBottom: 62,
        overflowY: isSafari ? 'scroll' : 'auto',
        '-webkit-overflow-scrolling': 'touch',
        '--scrollbar-size': '.375rem',
    },
    container2: {
        flex: 1,
        width: '100%',
        color: 'white',
        padding: 12,
        paddingBottom: 4,
        display: 'flex',
        flexDirection: 'row'
    },
    image: {
        width: 174,
        height: 174,
        margin: 28,
        objectFit: 'contain'
    },
    title: {
        alignSelf: 'flex-end',
        fontSize: 12,
        fontWeight: 400,
        backgroundColor: '#00BFFF',
        borderRadius: 5,
        paddingLeft: 6,
        paddingRight: 6,
    },
    content: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        overflow: 'hidden',
        minHeight: 60,
    },
    infoProduct: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 8,
    },
    price: {
        fontSize: 13,
        fontWeight: '600',
        color: PRIMARY_COLOR
    },
    desc2: {
        display: 'flex',
        flexDirection: 'column'
    },
    desc: {
        fontSize: 13,
        fontWeight: '600',
        color: 'rgba(0,0,0,0.5)'
    },
    nameProduct: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        fontSize: 13,
        fontWeight: 600,
        lineHeight: '18px',
    },
    container3: {
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 4,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnAuction: {
        fontSize: 13,
        fontWeight: 600,
        color: 'white',
        backgroundColor: PRIMARY_COLOR,
        letterSpacing: 0.1,
        borderRadius: 5,
        padding: 8,
        paddingLeft: 12,
        height: '100%',
        paddingRight: 12,
        marginLeft: 8,
        '&:hover': {
            border: `1px solid ${PRIMARY_COLOR}`,
            backgroundColor: 'white',
            color: PRIMARY_COLOR,
        }
    },
    inputInfo: {
        fontSize: 14,
        padding: '0px 6px 0px 6px'
    },
    formItem: {
        marginBottom: 4,
        marginTop: 16,
        display: 'flex',
        width: '100%',
        '& .ant-form-item-label label': {
            color: 'white',
            fontSize: 12,
        },
        '& .ant-form-item-explain-error': {
            fontSize: 10,
            color: 'white',
        }
    },
    btnBack: {
        color: PRIMARY_COLOR,
        '&:hover': {opacity: 0.6},
        '&:focus': {opacity: 0.6},
        marginBottom: 3,
    },
    labelContent: {
        flex: 1,
        fontWeight: 600,
        fontSize: 13,
        lineHeight: '18px',
        color: '#6A6A6A',
    },
    listOfParticipants: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        marginTop: 8,

    },
    listOfParticipants1: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: 6,
        '& span': {
            color: PRIMARY_COLOR,
            fontSize: 13,
            fontWeight: 700,
            lineHeight: '18px',
            letterSpacing: 0.1,
        }
    },
    participantItem: {
        display: 'flex',
        width: '100%',
        justifyContent: 'space-between',
        padding: '8px 0 8px 0',
        '& span': {
            fontSize: 13,
            fontWeight: 500,
            lineHeight: '18px',
        }
    },
    pagination: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    btnPagination: {
        border: '0.7px solid rgba(0,0,0,0.1)',
        width: 30,
        height: 30,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginRight: 8,
        marginTop: 8,
        color: 'rgba(0,0,0,0.6)',
        backgroundColor: 'white',
        '&:hover': {
            boxShadow: '1px 2px 6px #888888'
        },
        '&:focus': {
            opacity: 0.6,
        }
    },
}

export default auctionStyle;
