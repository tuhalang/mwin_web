const bannerStyle = {
    container: {
        height: 220,
        '& .slick-list': {
            height: 220
        }
    },
    image: {
        objectFit: 'contain',
        width: '100%',
    }
}

export default bannerStyle;
