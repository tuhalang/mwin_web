import {PRIMARY_COLOR} from "../../../consts";

const cardAuctionStyle = {
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        // height: 154,
        border: `1px solid ${PRIMARY_COLOR}`,
        borderRadius: 10,
        marginBottom: 12,
    },
    container2: {
        flex: 1,
        width: '100%',
        color: 'white',
        padding: 12,
        display: 'flex',
        flexDirection: 'column'
    },
    image: {
        width: 50,
        height: 50,
        margin: 10,
    },
    imageHotProduct: {
        width: 35,
        height: 35,
        objectFit: 'contain',
        marginRight: 8,
    },
    title: {
        alignSelf: 'flex-start',
        fontSize: 13,
        fontWeight: 600,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 3,
        paddingLeft: 7,
        paddingRight: 7,
        display: 'flex',
        alignItems: 'center',
        minHeight:20,
    },
    iconTitle: {
        width: 13,
        height: 13,
        '& path': {
            fill: 'white',
        },
        marginRight: 4,
    },
    content: {
        flex: 1,
        display: 'flex',

    },
    infoProduct: {
        display: 'flex',
        flexDirection: 'column',
    },
    price: {
        fontSize: 13,
        fontWeight: '500',
        color: PRIMARY_COLOR,
    },
    nameProduct: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        width: '100%',
        fontSize: 15.2,
        color: '#6A6A6A',
        fontWeight: 600,
    },
    container3: {
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 4,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnAuction: {
        fontSize: 13,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 15,
        color: 'white',
        border: 'none',
        paddingLeft: '3px 12px 3px 12px',
        '&:hover': {
            backgroundColor: 'white',
            border: `1px solid ${PRIMARY_COLOR}`,
            color: PRIMARY_COLOR,
        },
        '& span': {
            fontWeight: 600,
            letterSpacing: 0.1
        }
    },
    headCard: {
        display: 'flex',
        // justifyContent: 'space-between',
        justifyContent: 'flex-start'
    },
    timeHead: {
        color: '#6A6A6A',
        fontSize: 13,
        fontWeight: 500,
    }
}

export default cardAuctionStyle;
