import {PRIMARY_COLOR} from "../../../consts";

const cardAuctionWeekStyle = {
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        // height: 154,
        border: `1px solid ${PRIMARY_COLOR}`,
        borderRadius: 10,
        marginBottom: 12,
        padding: 12,
    },
    container2: {
        flex: 1,
        width: '100%',
        color: 'white',
        paddingLeft: 12,
        // paddingRight: 12,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8,
    },
    image: {
        width: 50,
        height: 50,
        marginRight: 8,
        objectFit: 'contain',
    },
    imageHotProduct: {
        width: 35,
        height: 35,
        objectFit: 'contain',
        marginRight: 8,
    },
    title: {
        alignSelf: 'flex-start',
        fontSize: 13,
        fontWeight: 600,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 3,
        paddingLeft: 7,
        paddingRight: 7,
        display: 'flex',
        alignItems: 'center',
        color: 'white',
        minHeight:20,
    },
    content: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',

    },
    infoProduct: {
        display: 'flex',
        flexDirection: 'column',
    },
    price: {
        fontSize: 13,
        fontWeight: '500',
        color: PRIMARY_COLOR,
    },
    nameProduct: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        width: '100%',
        fontSize: 15.2,
        color: '#6A6A6A',
        fontWeight: 600,
    },
    container3: {
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 4,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    labelTimeRemaining: {
        fontSize: 14,
        marginTop: 4,
        fontWeight: '400',
        marginRight: 6,
        color: 'white',
    },
    btnAuction: {
        fontSize: 13,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 15,
        color: 'white',
        border: 'none',
        paddingLeft: '3px 12px 3px 12px',
        '&:hover': {
            backgroundColor: 'white',
            border: `1px solid ${PRIMARY_COLOR}`,
            color: PRIMARY_COLOR,
        },
        '& span': {
            fontWeight: 600,
            letterSpacing: 0.1
        }
    },
    headCard: {
        display: 'flex',
        // justifyContent: 'space-between',
        justifyContent: 'flex-start'
    },
    timeHead: {
        color: '#6A6A6A',
        fontSize: 13,
        fontWeight: 500,
    },
    iconTitle: {
        width: 13,
        height: 13,
        '& path': {
            fill: 'white',
        },
        marginRight: 4,
    },
}

export default cardAuctionWeekStyle;
