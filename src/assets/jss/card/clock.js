import {PRIMARY_COLOR} from "../../../consts";

const clockStyle = {
    container: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 4,
    },
    containerItem: {
        display: 'flex',
        alignItems: 'center',
        '& span': {
            fontSize: 11,
            color: '#6A6A6A',
            marginBottom: -2,
            fontFamily: 'Montserrat',
        }
    },
    contentItem: {
        width: 20,
        height: 20,
        border: `1px solid ${PRIMARY_COLOR}`,
        borderRadius: 3,
        marginRight: 4,
        marginLeft: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: -4,
        '& span': {
            lineHeight: '18px',
            fontWeight: 500,
            color: PRIMARY_COLOR,
            fontSize: 11,
            fontFamily: 'Montserrat',
            letterSpacing: 0.1
        }
    },
    icon: {
        width: 20,
        height: 20,
        marginRight: 3,
    }
}

export default clockStyle;
