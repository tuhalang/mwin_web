import {PRIMARY_COLOR} from "../../consts";

const headerMobileStyle = {
    container: {
        width: '100%',
        backgroundColor: PRIMARY_COLOR,
        padding: 16,
        fontWeight: '400',
        display: 'flex',
        justifyContent: 'space-between',
        fontSize: 22,
    },
    title: {
        color: 'white',
        fontWeight: 600,
        fontSize: 16
    },
    icon: {
        color: 'white',
        height: '100%',
        width: 20,
        overflow: 'hidden',
        '&:hover': {
            opacity: 0.7,
        }
    }
}

export default headerMobileStyle;
