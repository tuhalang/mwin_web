import {PRIMARY_COLOR} from "../../../consts";
import {isMobile} from 'react-device-detect';

const historyItemStyle = {
    container: {
        width: '100%',
        height: isMobile ? 70 : 80,
        borderBottom: '1px solid rgba(0,0,0,0.2)',
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
    },
    image: {
        width: isMobile ? 50 : 70,
        height: isMobile ? 50 : 70,
        objectFit: 'contain',
    },
    divider: {
        height: '85% !important',
        borderLeftColor: 'rgba(0,0,0,0.08)'
    },
    content: {
        flex: 1,
        display: 'flex',
        justifyContent: 'space-between',
        height: '100%'
    },
    subContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: '100%'
    },
    time: {
        fontSize: isMobile ? 10 : 12,
        color: 'rgba(0,0,0,0.4)'
    },
    price: {
        fontSize: isMobile ? 11 : 13,
        fontWeight: '400',
        color: PRIMARY_COLOR,
    },
    nameProduct: {
        fontSize: 14,
        fontWeight: 500,
        width: 190,
        textOverflow: 'hidden',
        overflow: 'hidden',
        wordWrap: 'break-word',
        maxHeight: '1.5rem'
    },
    phone: {
        fontSize: isMobile ? 10 : 12,
        fontWeight: 400,
    },
    auctionId: {
        fontSize: isMobile ? 9 : 11,
        fontWeight: 400,
    }
}

export default historyItemStyle;
