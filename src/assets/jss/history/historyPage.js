import background from "../../img/light-blue-bg.jpg";
import {isMobile} from "react-device-detect";
import {PRIMARY_COLOR} from "../../../consts";


const historyPageStyle = {
    container: {
        backgroundImage: `url(${background})`,
        width: '100%',
        minHeight: '100vh',
        display: 'flex',
        justifyContent: 'center'
    },
    container2: {
        width: isMobile ? window.innerWidth : 400,
        height: '100vh',
        maxHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: 'white',
        position: 'relative',
    },
    containerStep1: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 16,
    },
    brand: {
        color: '#0e559f',
        textAlign: "center",
        marginBottom: '10vh',
    },
    title: {
        fontSize: "1.1rem",
        fontWeight: "500",
        display: "inline-block",
        position: "relative",
        color: PRIMARY_COLOR,
    },
    subtitle: {
        fontSize: "1.313rem",
        margin: "10px 0 0"
    },
    main: {
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        margin: "-60px 30px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    link: {
        textDecoration: "none"
    },
    textCenter: {
        textAlign: "center"
    },
    actions: {
        display: 'flex',
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
    },
    iconActions: {
        fontSize: 40,
        margin: 0,
    },
    action: {
        textAlign: "center",
        width: 170,
        height: 150,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        color: '#0e559f',
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.05)',
            boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
        },
        '&:active': {
            backgroundColor: 'rgba(0,0,0,0.1)',
        }
    },
    content: {
        flex: 1,
        paddingTop: 4,
        overflowY: 'auto',
        marginBottom: 65,
        paddingBottom: 8,
    },
    searchComponent: {
        display: 'flex',
        width: '100%',
        padding: 16,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    phoneSearch: {
        width: '100%',
        padding: 10,
        fontSize: 13,
        fontWeight: 500,
        lineHeight: '18px',
        color: PRIMARY_COLOR,
        '&:hover': {
            border: `1px solid ${PRIMARY_COLOR} !important`
        },
        '&:focus': {
            border: `1px solid ${PRIMARY_COLOR} !important`
        }
    },
    errorPhone: {
        color: 'red',
        fontSize: 12,
        height: 20,
    },
    btnSearch: {
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 6,
        border: 'none',
        width: '100%',
        marginTop: 4,
        '&:hover': {
            backgroundColor: PRIMARY_COLOR,
            color: 'white'
        },
        '&:focus': {
            backgroundColor: PRIMARY_COLOR,
            color: 'white'
        }
    },
    pagination: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    btnPagination: {
        border: '0.7px solid rgba(0,0,0,0.1)',
        width: 30,
        height: 30,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginRight: 8,
        marginTop: 8,
        color: 'rgba(0,0,0,0.6)',
        backgroundColor: 'white',
        '&:hover': {
            boxShadow: '1px 2px 6px #888888'
        },
        '&:focus': {
            opacity: 0.6,
        }
    },
    titleStep1: {
        fontSize: 13,
        fontWeight: 500,
        lineHeight: '17px',
        color: '#181725',
        width: 220,
        marginBottom: 18,
        textAlign: 'center',
    }
}

export default historyPageStyle;
