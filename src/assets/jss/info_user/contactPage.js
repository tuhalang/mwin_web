import {PRIMARY_COLOR} from "../../../consts";

const contactPageStyle = {
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        display: 'flex',
        padding: 16,
        paddingTop: 24,
    },
    icon: {
        fontSize: 24,
        color: PRIMARY_COLOR,
        marginRight: 8,
    },
    titleItem: {
        fontSize: 16,
        fontWeight: 400,
    },
    item: {
        marginTop: 10,
        marginBottom: 10,
        display: 'flex',
        alignItems: 'center',
        height: 70,
        boxShadow: '0px 2px 10px rgba(99, 24, 0, 0.1)',
        width: '100%',
        padding: 17,
    }
}

export default contactPageStyle;
