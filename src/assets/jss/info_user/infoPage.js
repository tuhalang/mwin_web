import background from "../../img/light-blue-bg.jpg";
import {isMobile, isSafari} from "react-device-detect";
import {BACKGROUND_COLOR, PRIMARY_COLOR} from "../../../consts";

const infoPageStyle = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        // marginTop: 21,
        width: '100%',
        alignItems: 'center',
        '& .ant-tabs-tab-active .ant-tabs-tab-btn': {
            color: PRIMARY_COLOR,
            fontWeight: 400,
        },
        '& .ant-tabs-tab-btn': {
            color: '#262626',
            fontWeight: 400,
        },
        '& .ant-tabs-nav-list': {
            width: '100%',
        },
        '& .ant-tabs': {
            flex: 1,
            backgroundColor: 'white',
            marginBottom: 62,
            maxHeight: `100%`,
            width: '100%',
            overflowY: isSafari ? 'scroll' : 'auto',
            "-webkit-overflow-scrolling": 'touch'
        },
        '& .ant-tabs-tab': {
            width: '50%',
            justifyContent: 'center'
        },
        '& .ant-tabs-ink-bar': {
            width: '50%',
            backgroundColor: PRIMARY_COLOR,
        },
        '& .ant-tabs-content-holder': {
            overflowY: 'auto',
        }
    },
    container2: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        paddingLeft: 12,
        paddingRight: 12,
        backgroundColor: PRIMARY_COLOR,
        paddingBottom: 4,
    },
    container3: {
        color: 'white',
        fontSize: 13,
        fontWeight: 600,
    },
    flags: {
        width: 30,
        height: 24,
        marginRight: 8,
        padding: '2px 4px 2px 4px',
        objectFit: 'contain',
        borderRadius: 2,
    },
    component: {
        backgroundColor: 'white',
        padding: '8px 16px 8px 16px',
        // marginTop: 4,
        marginBottom: 6,
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        cursor: 'pointer',
        borderBottom: '0.5px solid #E1E1E1',
    },
    btnNext: {
        alignSelf: 'flex-end',
        fontSize: 12,
        color: PRIMARY_COLOR,
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.1)',
        },
        '&:focus': {
            opacity: 0.7
        },
        width: 24,
        height: 24,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
    },
    icon2: {
        fontSize: 13,
        marginRight: 10,
        color: 'black',
    },
    title: {
        fontSize: 13,
        fontWeight: 500,
        lineHeight: '18px',
    }
}

export default infoPageStyle;
