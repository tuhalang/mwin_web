import background from "../../img/light-blue-bg.jpg";
import {isMobile} from "react-device-detect";
import {PRIMARY_COLOR} from "../../../consts";


const personalInfoPageStyle = {
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    container2: {
        height: '100%',
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        padding: 16,
    },
    containerStep1: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 16,
    },
    searchComponent: {
        display: 'flex',
        width: '100%',
        padding: 16,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    titleStep1: {
        fontSize: 13,
        fontWeight: 500,
        lineHeight: '17px',
        color: '#181725',
        width: 220,
        marginBottom: 18,
        textAlign: 'center',
    },
    phoneSearch: {
        width: '100%',
        padding: 10,
        fontSize: 13,
        fontWeight: 500,
        lineHeight: '18px',
        color: PRIMARY_COLOR,
        '&:hover': {
            border: `1px solid ${PRIMARY_COLOR} !important`
        },
        '&:focus': {
            border: `1px solid ${PRIMARY_COLOR} !important`
        }
    },
    errorPhone: {
        color: 'red',
        fontSize: 12,
        height: 20,
    },
    btnSearch: {
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 6,
        border: 'none',
        width: '100%',
        marginTop: 4,
        '&:hover': {
            backgroundColor: PRIMARY_COLOR,
            color: 'white'
        },
        '&:focus': {
            backgroundColor: PRIMARY_COLOR,
            color: 'white'
        }
    },
    info: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        alignItems: 'center',
        marginTop: 16,
    },
    info2: {
        display: 'flex',
        marginBottom: 6,
        marginTop: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        borderBottom: '0.5px solid #E2E2E2',
    },
    info3: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
    },
    labelInfo: {
        fontSize: 13,
        fontWeight: 400,
        color: PRIMARY_COLOR,
        width: 90
    },
    inputInfo: {
        fontSize: 13,
        fontWeight: 400,
    },
    dgn: {
        fontSize: 13,
        fontWeight: 400,
        color: PRIMARY_COLOR,
    },
    btnCancel: {
        width: '100%',
        backgroundColor: '#EA3D32',
        border: 'none',
        marginTop: 40,
        '&:hover': {
            backgroundColor: '#EA3D32',
        },
        '&:focus': {
            backgroundColor: '#EA3D32',
        },
        '& span': {
            fontSize: 13,
            fontWeight: 600,
            lineHeight: '18px'
        }
    }
}

export default personalInfoPageStyle;
