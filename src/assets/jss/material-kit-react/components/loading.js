import {PRIMARY_COLOR} from "../../../../consts";

const loadingStyle = {
    container: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
        zIndex: 10000,
    },
    content: {
        width: 310,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& span': {
            color: 'white',
        }
    },
    linear: {
        width: '100%',
        marginTop: 4,
        backgroundColor: 'white',
        '& .MuiLinearProgress-barColorPrimary': {
            backgroundColor: PRIMARY_COLOR
        }
    }
}

export default loadingStyle;
