import {container} from "assets/jss/material-kit-react.js";
import {blackColor} from "../components/headerLinksStyle";

const componentsMobileStyle = {
    container,
    brand: {
        color: '#0e559f',
        textAlign: "center",
        marginBottom: '5vh',
    },
    title: {
        fontSize: "2.5rem",
        fontWeight: "600",
        display: "inline-block",
        position: "relative"
    },
    subtitle: {
        fontSize: "1.313rem",
        margin: "10px 0 0"
    },
    main: {
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        margin: "-60px 30px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    link: {
        textDecoration: "none"
    },
    textCenter: {
        textAlign: "center"
    },
    actions: {
        display: 'flex',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection: 'column',
    },
    iconActions: {
        fontSize: 25,
        marginRight: 16,
    },
    action: {
        textAlign: "center",
        width: '80vw',
        height: 60,
        marginTop: 8,
        marginBottom: 8,
        paddingRight: 16,
        paddingLeft: 16,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex- start',
        alignItems: 'center',
        borderRadius: 10,
        color: '#0e559f',
        backgroundColor: 'white',
        boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.05)',
            boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
            cursor: 'pointer'
        },
        '&:active': {
            backgroundColor: 'rgba(0,0,0,0.1)',
        }
    }
};

export default componentsMobileStyle;
