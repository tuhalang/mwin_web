import utils from "../../../common/utils";

const productStyle = {
    imgRaised: {
        boxShadow:
            "0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    imgRounded: {
        borderRadius: "6px !important"
    },
    imgFluid: {
        maxWidth: "100%",
        height: "550px",
        objectFit: 'contain'
    },
    container: {
        marginTop: 150
    },
    imageProduct: {
        display: 'flex',
        justifyContent: 'flex-end',
        padding: 0,
    },
    titleProduct: {
        fontSize: '2em',
        margin: '0 0 17.5px',
        fontFamily: '"Swiss 721 Rounded", sans-serif',
        fontStyle: 'normal',
        fontWeight: 700,
        lineHeight: 1.2,
        overflowWrap: 'break-word',
        wordWrap: 'break-word',
        color: '#3a3a3a',
    },
    priceProduct: {
        width: 'auto!important',
        fontSize: '1.5em',
        fontWeight: '600',
        color: '#0e559f',
        lineHeight: '1.8!important',
    },
    infoProduct: {
        display: 'flex',
        flexDirection: 'column',
        width: utils.getWindowDimensions().width <1200?'100%': 600,
        paddingRight: utils.getWindowDimensions().width < 1200 ? 30 : 0,
        paddingLeft: 30
    },
    formLabel: {
        '& .ant-form-item-label': {
            textAlign: 'left'
        }
    },
    formInfo: {
        marginTop: 50
    },
    auctionBtn: {
        width: '100%',
        fontSize: '20px',
        height: '40px',
    }
}

export default productStyle;
