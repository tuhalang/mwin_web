import utils from "../../../common/utils";

const productStyle = {
    imgRaised: {
        // boxShadow:
        //     "0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
        backgroundColor: 'transparent'
    },
    imgRounded: {
        // borderRadius: "6px !important"
    },
    imgFluid: {
        width: '90%',
        objectFit: 'contain',
    },
    container: {
        paddingTop: 80,
        backgroundColor: 'white',
        minHeight: '100vh',
        overflowY: 'scroll',
    },
    imageProduct: {
        padding: 0,
        height: utils.getWindowDimensions().height - 80,
        zIndex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    titleProduct: {
        fontSize: '1.6em',
        margin: '4px 0 0',
        fontFamily: '"Swiss 721 Rounded", sans-serif',
        fontStyle: 'normal',
        fontWeight: 700,
        lineHeight: 1.2,
        overflowWrap: 'break-word',
        wordWrap: 'break-word',
        color: '#3a3a3a',
        flex: 1
    },
    priceProduct: {
        width: 'auto!important',
        fontSize: '1.2em',
        fontWeight: '600',
        color: '#0e559f',
        lineHeight: '1.8!important',
    },
    infoProduct: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        width: utils.getWindowDimensions().width < 1200 ? '100%' : 600,
        backgroundImage: 'linear-gradient(transparent,white 75%)',
        zIndex: 2,
        position: 'absolute',
        height: utils.getWindowDimensions().height - 80,
    },
    formLabel: {
        marginBottom: 8,
        '& .ant-form-item-label': {
            textAlign: 'left',
            height: 28,
        },
    },
    formInfo: {
        display: 'flex',
        flexDirection: 'column',
    },
    auctionBtn: {
        width: '100%'
    },
    infoNamePrice: {
        display: 'flex',
        width: '100%',
    },
    productDetail: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 10,
        padding: 8,
        fontWeight: '400'
    }
}

export default productStyle;
