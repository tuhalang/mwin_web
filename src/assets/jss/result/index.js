import background from "../../img/light-blue-bg.jpg";
import {isMobile, isSafari} from "react-device-detect";
import {BACKGROUND_COLOR, PRIMARY_COLOR} from "../../../consts";

const resultPageStyle = {
    container: {
        backgroundImage: `url(${background})`,
        width: '100%',
        minHeight: '100vh',
        display: 'flex',
        justifyContent: 'center'
    },
    container2: {
        width: isMobile ? window.innerWidth : 400,
        height: '100vh',
        maxHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: BACKGROUND_COLOR,
        overflowY: 'auto',
        position: 'relative',
        '& .ant-tabs-tab-active .ant-tabs-tab-btn': {
            color: PRIMARY_COLOR,
            fontWeight: 400,
        },
        '& .ant-tabs-tab-btn': {
            color: '#262626',
            fontWeight: 400,
        },
        '& .ant-tabs-nav-list': {
            width: '100%',
        },
        '& .ant-tabs': {
            flex: 1,
            backgroundColor: 'white',
            marginBottom: 62,
            maxHeight: `100%`,
            overflowY: isSafari ? 'scroll' : 'auto',
            "-webkit-overflow-scrolling": 'touch'
        },
        '& .ant-tabs-tab': {
            width: '50%',
            justifyContent: 'center'
        },
        '& .ant-tabs-ink-bar': {
            width: '50%',
            backgroundColor: PRIMARY_COLOR,
        },
        '& .ant-tabs-content-holder': {
            overflowY: 'auto',
        }
    },
    tabBarStyle: {
        color: PRIMARY_COLOR,
        fontWeight: 400,
    }
}

export default resultPageStyle;
