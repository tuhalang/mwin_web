import {isSafari} from "react-device-detect";

const onGoingSessionStyle = {
    container: {},
    content: {
        flex: 1,
        padding: 16,

    }
}

export default onGoingSessionStyle;
