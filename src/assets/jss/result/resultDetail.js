import {PRIMARY_COLOR} from "../../../consts";

const resultDetailStyle = {
    container: {
        flex: 1,
        marginBottom: 62,
        overflowY: 'auto',
        backgroundColor: 'white',
        '& .ant-table-thead': {
            fontSize: 12,
        }
    },
    pagination: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 6,
    },
    btnPagination: {
        border: '0.7px solid rgba(0,0,0,0.1)',
        width: 30,
        height: 30,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginRight: 8,
        marginTop: 8,
        color: 'rgba(0,0,0,0.6)',
        backgroundColor: 'white',
        '&:hover': {
            boxShadow: '1px 2px 6px #888888'
        },
        '&:focus': {
            opacity: 0.6,
        }
    },
    header: {
        fontSize: 13,
        color: PRIMARY_COLOR,
        display: 'flex',
        paddingLeft: 6,
        justifyContent: 'space-between',
        '& span': {
            fontWeight: 600,
        }
    },
    tabBar: {
        width: '100%',
        height: 35,
        marginTop: 16,
        display: 'flex',
        borderTop: `1px solid ${PRIMARY_COLOR}`,
        borderBottom: `1px solid ${PRIMARY_COLOR}`,
        '& span': {
            fontSize: 13,
            lineHeight: '32px',
            flex: 1,
            textAlign: 'center',
            fontWeight: '500'
        }
    },
    tab: {
        color: '#6A6A6A'
    },
    tabActive: {
        color: 'white',
        backgroundColor: PRIMARY_COLOR,
    }
}

export default resultDetailStyle;
