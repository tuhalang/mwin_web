import {PRIMARY_COLOR} from "../../../consts";

const resultItemStyle = {
    container: {
        backgroundColor: '#FFF9F3',
        display: 'flex',
        padding: '6px 12px 6px 12px',
        marginTop: 10,
    },
    image: {
        width: 68,
        height: 68,
        objectFit: 'contain',
        marginRight: 7,
    },
    content: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        '& span': {
            fontSize: 13,
            fontWeight: 400,
            lineHeight: '18px',
            color: '#6A6A6A',
        }
    },
    productName: {
        color: `${PRIMARY_COLOR} !important`,
        fontWeight: '500 !important',
        flex: 1,
    },
    price: {
        fontSize: 13,
        color: PRIMARY_COLOR,
        lineHeight: '17px',
        fontWeight: 500,
    }
}

export default resultItemStyle;
