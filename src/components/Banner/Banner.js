import React, {Component, useRef} from "react";
import styles from "../../assets/jss/banner";
import withStyles from "@material-ui/core/styles/withStyles";
import Slider from "react-slick";
import _ from 'lodash';
import utils from "../../common/utils";


class Banner extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const
            settings = {
                dots: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1700
            };
        const {classes} = this.props;
        const banners = this.props.images;

        return (

            <div className={classes.container}>
                <Slider ref={slider => (this.slider = slider)} {...settings}>
                    {_.map(banners, e => <div key={e.imageUrl}>
                        <img src={utils.getImageUrl(e.imageUrl)} alt={e.imageTitle} className={classes.image}/>
                    </div>)}
                </Slider>
            </div>
        )
    }
}

export default withStyles(styles)(Banner);
