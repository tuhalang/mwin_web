import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/card/cardAuction";
import FlipClock from "../FlipClock/FlipClock";
import utils from "../../common/utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {Button} from "antd";
import {useTranslation} from "react-i18next";
import {ReactComponent as PlayIcon} from 'assets/img/29-auction.svg';
import Clock from "../Clock/Clock";
import {CURRENCY_UNIT} from "../../consts";

const useStyles = makeStyles(styles);

function CardAuction(props) {
    const classes = useStyles();
    const {data} = props;
    const {t, i18n} = useTranslation('translation');
    const {
        auctionSessionId,
        startDate,
        endDate,
        productId,
        status,
        productName,
        productDesc,
        imageUrl,
        price,
        sessionName,
        remainTime,
        sessionType,
        sessionCode,
        productCode
    } = data.lstProduct[0];



    return (
        <div className={classes.container}>
            <div className={classes.container2}>
                <div className={classes.headCard}>
                    <div className={classes.title}><PlayIcon className={classes.iconTitle}/> {sessionName}</div>
                </div>
                <div className={classes.content}>
                    <img src={utils.getImageUrl(imageUrl) || ''} className={classes.image}/>
                    <div style={{display: 'flex', justifyContent: "space-between", flex: 1, alignItems: 'center'}}>
                        <div className={classes.infoProduct}>
                    <span
                        className={classes.nameProduct}>{utils.ellipsisString(productName || '', 25)}</span>
                            <span className={classes.price}>{`${price} ${CURRENCY_UNIT}`}</span>
                        </div>
                        <Button className={classes.btnAuction} onClick={() => {
                            props.callback(data.lstProduct[0])
                        }}>{t('auction')} <FontAwesomeIcon icon={faChevronRight} style={{marginLeft: 4,}}/></Button>
                    </div>
                </div>
                <Clock time={utils.parseDateFromString(endDate)}/>
            </div>
        </div>
    )
}

export default CardAuction;
