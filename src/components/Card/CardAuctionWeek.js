import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/card/cardAuctionWeek";
import FlipClock from "../FlipClock/FlipClock";
import utils from "../../common/utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {Button} from "antd";
import {Divider} from "@material-ui/core";
import Clock from "../Clock/Clock";
import {ReactComponent as PlayIcon} from "../../assets/img/29-auction.svg";
import {CURRENCY_UNIT, PRIMARY_COLOR} from "../../consts";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles(styles);

function CardAuctionWeek(props) {
    const classes = useStyles();
    const {data} = props;
    const listProduct = data.lstProduct || [];
    const {t} = useTranslation('translation');

    return (
        <div className={classes.container}>
            <div className={classes.headCard}>
                <div className={classes.title}><PlayIcon className={classes.iconTitle}/> {data.sessionName}</div>
            </div>
            <div className={classes.container2}>
                <div>
                    <img src={utils.getImageUrl(listProduct[0].imageUrl) || ''}
                         className={classes.image}/>
                </div>
                <div className={classes.content}>
                    <div className={classes.infoProduct}>
                    <span
                        className={classes.nameProduct}>{utils.ellipsisString(listProduct[0].productName, 24)}</span>
                        <span className={classes.price}>{`${listProduct[0].price} ${CURRENCY_UNIT}`}</span>
                    </div>
                </div>
                <Button className={classes.btnAuction}
                        onClick={() => props.callback(listProduct[0])}>{t('auction')} <FontAwesomeIcon
                    icon={faChevronRight}
                    style={{marginLeft: 4,}}/></Button>
            </div>
            <Divider style={{marginTop: 8, marginBottom: 8, backgroundColor: PRIMARY_COLOR}}/>
            <div className={classes.container2}>
                <div>
                    <img src={utils.getImageUrl(listProduct[1].imageUrl) || ''}
                         className={classes.image}/>
                </div>
                <div className={classes.content}>
                    <div className={classes.infoProduct}>
                    <span
                        className={classes.nameProduct}>{utils.ellipsisString(listProduct[1].productName, 24)}</span>
                        <span className={classes.price}>{`${listProduct[1].price} ${CURRENCY_UNIT}`}</span>
                    </div>
                </div>
                <Button className={classes.btnAuction}
                        onClick={() => props.callback(listProduct[1])}>{t('auction')} <FontAwesomeIcon
                    icon={faChevronRight}
                    style={{marginLeft: 4,}}/></Button>
            </div>
            <Clock style={{marginTop: 12}} time={utils.parseDateFromString(data.endDate)}/>
        </div>
    )
}

export default CardAuctionWeek;
