import React, {useEffect, useState} from "react";
import {ReactComponent as ClockIcon} from 'assets/img/27-clock.svg';
import styles from 'assets/jss/card/clock';
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles(styles);


function ClockItem(props) {
    const classes = useStyles();
    const {title, value} = props;
    return (
        <div className={classes.containerItem}>
            <div className={classes.contentItem}>
                <span>{value}</span>
            </div>
            <span>{title}</span>
        </div>
    )
}

function Clock(props) {
    const classes = useStyles();
    const [time, setTime] = useState(props.time.getTime())
    const [days, setDays] = useState(0);
    const [hours, setHours] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [second, setSecond] = useState(0);
    const {t} = useTranslation('translation');
    const {isRunning} = props;

    const updateTime = async () => {
        if (isRunning !== false) {
            let t = time - new Date().getTime();
            const d = Math.round(t / (1000 * 60 * 60 * 24)) > 0 ? Math.round(t / (1000 * 60 * 60 * 24)) : 0;
            t = t % (1000 * 60 * 60 * 24);
            const h = Math.round(t / (1000 * 60 * 60)) > 0 ? Math.round(t / (1000 * 60 * 60)) : 0;
            t = t % (1000 * 60 * 60);
            const m = Math.round(t / (1000 * 60)) > 0 ? Math.round(t / (1000 * 60)) : 0;
            t = t % (1000 * 60);
            const s = Math.round(t / 1000) > 0 ? Math.round(t / 1000) : 0;
            setTime(t);
            if (d !== days) {
                setDays(d)
            }
            if (h !== hours) {
                setHours(h)
            }
            if (m !== minutes) {
                setMinutes(m)
            }
            if (s !== second) {
                setSecond(s)
            }
        }
    }

    useEffect(() => {
        const interval = setInterval(() => updateTime(), 1000);
        return () => clearInterval(interval);
    }, []);

    return (
        <div className={classes.container} {...props}>
            <ClockIcon className={classes.icon}/>
            <ClockItem title={t('days')} value={days}/>
            <ClockItem title={t('hours')} value={hours}/>
            <ClockItem title={t('minutes')} value={minutes}/>
            <ClockItem title={t('second')} value={second}/>
        </div>
    );
}

export default Clock;
