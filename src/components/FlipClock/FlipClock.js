import React from 'react';
import '../../assets/css/flip_styles.scss'
import _ from 'lodash';
import {withTranslation} from "react-i18next";
// function component
const AnimatedCard = ({animation, digit}) => {
    return (
        <div className={`flipCard ${animation}`}>
            <span>{digit}</span>
        </div>
    );
};

// function component
const StaticCard = ({position, digit}) => {
    return (
        <div className={position}>
            <span>{digit}</span>
        </div>
    );
};

const FlipUnitContainer = ({digit, shuffle, unit}) => {

    // assign digit values
    let currentDigit = digit;
    let previousDigit = digit - 1;

    // to prevent a negative value
    if (unit !== 'hours') {
        previousDigit = previousDigit === -1
            ? 59
            : previousDigit;
    }
    if (unit !== 'days') {
        previousDigit = previousDigit === -1
            ? 0
            : previousDigit;
    } else {
        previousDigit = previousDigit === -1
            ? 23
            : previousDigit;
    }

    // add zero
    if (currentDigit < 10) {
        currentDigit = `0${currentDigit}`;
    }
    if (previousDigit < 10) {
        previousDigit = `0${previousDigit}`;
    }

    // shuffle digits
    const digit1 = shuffle
        ? previousDigit
        : currentDigit;
    const digit2 = !shuffle
        ? previousDigit
        : currentDigit;

    // shuffle animations
    const animation1 = shuffle
        ? 'fold'
        : 'unfold';
    const animation2 = !shuffle
        ? 'fold'
        : 'unfold';

    return (
        <div className={'flipUnitContainer'}>
            <StaticCard
                position={'upperCard'}
                digit={currentDigit}
            />
            <StaticCard
                position={'lowerCard'}
                digit={previousDigit}
            />
            <AnimatedCard
                digit={digit1}
                animation={animation1}
            />
            <AnimatedCard
                digit={digit2}
                animation={animation2}
            />
        </div>
    );
};

// class component
class FlipClock extends React.Component {

    constructor(props) {
        super(props);
        this.time = this.props.input || {};
        this.state = {
            days: this.time.getDay() || 0,
            daysShuffle: true,
            hours: this.time.getHours() || 0,
            hoursShuffle: true,
            minutes: this.time.getMinutes() || 0,
            minutesShuffle: true,
            seconds: this.time.getSeconds() || 0,
            secondsShuffle: true,
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.updateTime(),
            1000
        );

    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    async updateTime() {
        this.time = new Date(this.props.input.getTime() - new Date().getTime());
        // set time units
        const days = this.time.getDay();
        const hours = this.time.getHours();
        const minutes = this.time.getMinutes();
        const seconds = this.time.getSeconds();
        // on day chanage, update days and shuffle state
        if (days !== this.state.days) {
            const daysShuffle = !this.state.daysShuffle;
            this.setState({
                days,
                daysShuffle
            });
        }
        // on hour chanage, update hours and shuffle state
        if (hours !== this.state.hours) {
            const hoursShuffle = !this.state.hoursShuffle;
            this.setState({
                hours,
                hoursShuffle
            });
        }
        // on minute chanage, update minutes and shuffle state
        if (minutes !== this.state.minutes) {
            const minutesShuffle = !this.state.minutesShuffle;
            this.setState({
                minutes,
                minutesShuffle
            });
        }
        // on second chanage, update seconds and shuffle state
        if (seconds !== this.state.seconds) {
            const secondsShuffle = !this.state.secondsShuffle;
            this.setState({
                seconds,
                secondsShuffle
            });
        }
    }

    render() {
        const {
            days,
            hours,
            minutes,
            seconds,
            daysShuffle,
            hoursShuffle,
            minutesShuffle,
            secondsShuffle
        } = this.state;
        const {t} = this.props;


        const {isLabel} = this.props;

        return (
            <div className={'flipClock'} {...this.props}>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <FlipUnitContainer
                        unit={'days'}
                        digit={days}
                        shuffle={daysShuffle}
                    />
                    {isLabel ? <span className={'label-clock'}>{t('days')}</span> : <div/>}
                </div>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <FlipUnitContainer
                        unit={'hours'}
                        digit={hours}
                        shuffle={hoursShuffle}
                    />
                    {isLabel ? <span className={'label-clock'}>{t('hours')}</span> : <div/>}
                </div>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <FlipUnitContainer
                        unit={'minutes'}
                        digit={minutes}
                        shuffle={minutesShuffle}
                    />
                    {isLabel ? <span className={'label-clock'}>{t('minutes')}</span> : <div/>}
                </div>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <FlipUnitContainer
                        unit={'seconds'}
                        digit={seconds}
                        shuffle={secondsShuffle}
                    />
                    {isLabel ? <span className={'label-clock'}>{t('second')}</span> : <div/>}
                </div>
            </div>
        );
    }
}

export default withTranslation('translation')(FlipClock);
