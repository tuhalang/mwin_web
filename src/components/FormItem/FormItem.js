import React from "react";
import {Col, Input, Row} from "antd";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/form/FormItem";


const useStyles = makeStyles(styles);

function FormItem(props) {
    const classes = useStyles();
    const {label, placeholder, errorMessage, name, require, reset, value, disabled} = props;
    return (
        <div style={{display: 'flex', flexDirection: "column", flex: 1, width: '100%'}}>
            <Input placeholder={placeholder || ''} name={name} className={classes.inputFormItem} onFocus={reset}
                   defaultValue={value || ''} disabled={disabled}/>
            <span className={classes.errorMessage}>{errorMessage || ''}</span>
        </div>
    )
}

export default FormItem;
