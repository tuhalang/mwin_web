import React from "react";
import {Col, Input, Row} from "antd";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/form/FormItem";


const useStyles = makeStyles(styles);

function PriceItem(props) {
    const classes = useStyles();
    const {placeholder, name, require, reset, value, disabled} = props;
    return (
        <div style={{flex: 1}}>
            <Input placeholder={placeholder || ''} name={name} className={classes.inputFormItem} onFocus={reset}
                   defaultValue={value || ''} disabled={disabled}
                   required={require}
                   style={{
                       textAlign: 'left'
                   }}/>
        </div>
    )
}

export default PriceItem;
