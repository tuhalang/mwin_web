import React from "react";
import Poppers from "@material-ui/core/Popper";
import {Link} from "react-router-dom";
import classNames from "classnames";
import {hexToRgb, makeStyles} from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import {Apps, CloudDownload} from "@material-ui/icons";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";
import {ClickAwayListener, Grow, MenuItem, MenuList, Paper} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {faCheckCircle, faGavel, faGlobe, faClock, faUserAlt} from "@fortawesome/free-solid-svg-icons";
import {blackColor, primaryColor, whiteColor} from '../../assets/jss/material-kit-react/components/headerLinksStyle'

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
    const classes = useStyles();
    const [openAuction, setOpenAuction] = React.useState(false);
    const {t, i18n} = useTranslation();

    const handleClickNotification = event => {
        setOpenAuction(!openAuction)
    };

    const handleLanguage = (lng = '') => {
        setOpenAuction(false)
        i18n.changeLanguage(lng == '' ? 'en' : lng);
    };


    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <Button
                    color="transparent"
                    href={'/product-page'}
                    target={'_self'}
                    className={classes.navLink}
                >
                    <FontAwesomeIcon className={classes.icons} icon={faGavel}/> Đặt giá
                </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Button
                    color="transparent"
                    className={classes.navLink}
                >
                    <FontAwesomeIcon className={classes.icons} icon={faCheckCircle}/> Kết quả
                </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Button
                    color="transparent"
                    className={classes.navLink}
                >
                    <FontAwesomeIcon className={classes.icons} icon={faClock}/> Sắp đấu giá
                </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
                <CustomDropdown
                    noLiPadding
                    buttonText="More"
                    buttonProps={{
                        className: classes.navLink,
                        color: "transparent"
                    }}
                    buttonIcon={Apps}
                    dropdownList={[
                        <Link to="/" className={classes.dropdownLink}>
                            Lịch sử đấu giá
                        </Link>,
                        <Link to="/" className={classes.dropdownLink}>
                            Thông tin thuê bao
                        </Link>,
                    ]}
                />
            </ListItem>
            <ListItem className={classes.listItem}>
                <div className={classes.manager}>
                    <Button
                        color={window.innerWidth > 959 ? "transparent" : "white"}
                        justIcon={window.innerWidth > 959}
                        simple={!(window.innerWidth > 959)}
                        aria-owns={openAuction ? "language-menu-list-grow" : null}
                        aria-haspopup="true"
                        onClick={handleClickNotification}
                        className={classes.buttonLink}
                    >
                        <FontAwesomeIcon icon={faGlobe}/>
                    </Button>
                    <Poppers
                        open={openAuction}
                        anchorEl={openAuction}
                        transition
                        disablePortal
                        className={
                            classNames({[classes.popperClose]: !openAuction}) +
                            " " +
                            classes.popperNav
                        }
                        style={{
                            marginTop: 50
                        }}
                    >
                        {({TransitionProps, placement}) => (
                            <Grow
                                {...TransitionProps}
                                id="language-menu-list-grow"
                                style={{
                                    transformOrigin:
                                        placement === "bottom" ? "center top" : "center bottom",
                                    backgroundColor: '#eeebeb'
                                }}
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={() => setOpenAuction(false)}>
                                        <MenuList role="menu">
                                            <MenuItem
                                                onClick={() => handleLanguage('en')}
                                                className={classes.dropdownItem}
                                                style={i18n.language.startsWith('en') ? {
                                                    backgroundColor: primaryColor[0],
                                                    color: whiteColor,
                                                    boxShadow:
                                                        "0 4px 20px 0 rgba(" +
                                                        hexToRgb(blackColor) +
                                                        ",.14), 0 7px 10px -5px rgba(" +
                                                        hexToRgb(primaryColor[0]) +
                                                        ",.4)"
                                                } : {}}
                                            >
                                                {t('en')}
                                            </MenuItem>
                                            <MenuItem
                                                onClick={() => handleLanguage('vi')}
                                                className={classes.dropdownItem}
                                                style={i18n.language.startsWith('vi') ? {
                                                    backgroundColor: primaryColor[0],
                                                    color: whiteColor,
                                                    boxShadow:
                                                        "0 4px 20px 0 rgba(" +
                                                        hexToRgb(blackColor) +
                                                        ",.14), 0 7px 10px -5px rgba(" +
                                                        hexToRgb(primaryColor[0]) +
                                                        ",.4)"
                                                } : {}}
                                            >
                                                {t('vi')}
                                            </MenuItem>
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Grow>
                        )}
                    </Poppers>
                </div>
            </ListItem>
        </List>
    );
}
