import React from "react";
import _ from 'lodash';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/headerMobile";
import {faChevronLeft, faGavel} from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles(styles);

function HeaderMobile(props) {
    const classes = useStyles();
    const {title, icon, onBackPage} = props;

    return (
        <div className={classes.container}>
            <div className={classes.icon} onClick={onBackPage}>{
                icon ? <FontAwesomeIcon icon={faChevronLeft} size={11}/> : <div/>
            }</div>
            <span className={classes.title}>{title}</span>
            <div className={classes.icon}></div>
        </div>
    )
}

export default HeaderMobile;
