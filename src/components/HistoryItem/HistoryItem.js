// import React from "react";
// import styles from 'assets/jss/history/historyItem'
// import {makeStyles} from "@material-ui/core/styles";
// import {Divider} from "antd";
//
// const useStyles = makeStyles(styles)
//
// function HistoryItem(props) {
//     const {data} = props;
//     const classes = useStyles();
//     return (
//         <div className={classes.container}>
//             <img src={require('assets/img/keno_logo_white.png')} className={classes.image}/>
//             <Divider type={'vertical'} className={classes.divider}/>
//             <div className={classes.content}>
//                 <div className={classes.subContent}>
//                     <span className={classes.nameProduct}>{data.productName}</span>
//                     <span className={classes.phone}>Phone number: <span
//                         style={{color: 'deepSkyBlue'}}>{data.msisdn}</span></span>
//                     <span className={classes.time}>{data.auctionDate}</span>
//                 </div>
//                 <div className={classes.subContent} style={{alignItems: 'flex-end',}}>
//                     <span className={classes.auctionId}>Code: {data.auctionCode}</span>
//                     {/*<span className={classes.price}>Giá đấu: 10,000$</span>*/}
//                     <span className={classes.price}>Giá: {data.priceValue} $</span>
//                 </div>
//             </div>
//         </div>
//     )
// }
//
// export default HistoryItem;

import React from "react";
import styles from 'assets/jss/result/resultItem';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import consts, {CURRENCY_UNIT} from "../../consts";
import {useTranslation} from "react-i18next";
import utils from "../../common/utils";


const useStyles = makeStyles(styles);

function HistoryItem(props) {
    const classes = useStyles();
    const {data} = props;
    const {t} = useTranslation('history');

    return (
        <div className={classes.container}>
            <img src={utils.getImageUrl(data.imageUrl || '')} className={classes.image}/>
            <div className={classes.content}>
                <span className={classes.productName}>{t('product-label')}: {data.productName}</span>
                <span>{t('phone-number-label')}: {data.msisdn}</span>
                <span><FontAwesomeIcon icon={faClock}/> {data.auctionDate}</span>
            </div>
            <span className={classes.price}>{`${data.priceValue} ${CURRENCY_UNIT}`}</span>
        </div>
    )
}

export default HistoryItem;

