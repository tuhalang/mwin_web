import React from "react";
import styles from 'assets/jss/material-kit-react/components/loading'
import {makeStyles} from "@material-ui/core/styles";
import {LinearProgress} from "@material-ui/core";
import {PRIMARY_COLOR} from "../../consts";


const useStyles = makeStyles(styles);

function LinearLoading(props) {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <div className={classes.content}>
                <span>Loading...</span>
                <LinearProgress className={classes.linear}/>
            </div>
        </div>
    )
}

export default LinearLoading;
