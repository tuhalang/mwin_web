import React from 'react';
import '../../assets/css/dot.css'

function Loading() {
    return (
        <div>
            <div className={'container-dot'}>
            <span className="dots-cont">
                    <span className="dot dot-1"></span>
                    <span className="dot dot-2"></span>
                    <span className="dot dot-3"></span>
                </span>
            </div>
        </div>
    );
}

export default Loading;
