import React, {useState} from "react";
import {Button, Modal} from "antd";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/modal";
import 'assets/css/modal_repair.css';
import {useTranslation} from "react-i18next";
import {PRIMARY_COLOR} from "../../consts";

const useStyles = makeStyles(styles);

function RegisterServiceModal(props) {
    const classes = useStyles();
    const {visible, handleOk, handleCancel, phone} = props;
    const [loading, setLoading] = useState(false);
    const {t} = useTranslation('play');

    return (
        <Modal title=""
               destroyOnClose={true}
               visible={visible}
               onOk={handleOk}
               onCancel={handleCancel}
               width={220}
               closable={false}
               bodyStyle={{borderRadius: 10}}
               footer={[
                   <div style={{display: 'flex', justifyContent: 'space-evenly'}}>
                       <Button key="back" onClick={handleCancel}
                               style={{
                                   backgroundColor: 'white',
                                   border: `1.5px solid ${PRIMARY_COLOR}`,
                                   color: PRIMARY_COLOR,
                                   lineHeight: '18px',
                               }} className={classes.btnModal}>
                           {t('cancel-btn')}
                       </Button>
                       <Button key="submit" type="primary" loading={loading} onClick={handleOk}
                               style={{backgroundColor: PRIMARY_COLOR}} className={classes.btnModal}>
                           {t('register-btn')}
                       </Button>
                   </div>
               ]}
        >
            <div style={{textAlign: 'center',}}>
                <span style={{
                    fontSize: 13,
                    lineHeight: '18px',
                    color: '#181725',
                    fontWeight: 600
                }}>{t('sign-up-to-continue-title')}</span>
            </div>
        </Modal>
    )
}

export default RegisterServiceModal;
