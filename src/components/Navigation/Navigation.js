import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/navigation.js";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import _ from 'lodash';
import {faAward, faHistory, faPlayCircle, faUserAlt, faUserCircle} from "@fortawesome/free-solid-svg-icons";
import {useTranslation} from "react-i18next";
import {ReactComponent as PlayIcon} from 'assets/img/29-auction.svg';
import {ReactComponent as ResultIcon} from 'assets/img/28-result.svg';
import {Link} from "react-router-dom";

const useStyles = makeStyles(styles);

function Navigation(props) {

    const classes = useStyles();
    const {callback} = props;
    const [active, setActive] = useState('play');
    const {t, i18n} = useTranslation('translation');

    console.log(active, "active")

    const defaultRoutes = [
        {
            icon: PlayIcon,
            type: 'image',
            title: t('play'),
            id: 'play'
        },
        {
            icon: ResultIcon,
            type: 'image',
            title: t('result'),
            id: 'result'
        },
        {
            icon: faHistory,
            type: 'fontawesome',
            title: t('history'),
            id: 'history'
        }, {
            icon: faUserCircle,
            type: 'fontawesome',
            title: t('info'),
            id: 'info'
        }];
    const routes = props.routes || defaultRoutes;


    useEffect(() => {
        if (props.active && props.active != '')
            setActive(props.active)
    }, [props.active])

    return (
        <div className={classes.container} {...props}>
            {_.map(routes, e => <Link key={e.id} className={classes.item} to={'/' + e.id} onClick={() => {
                setActive(e.id)
            }}>
                {e.type == 'fontawesome' ? <FontAwesomeIcon icon={e.icon}

                                                            className={active == e.id ? classes.iconActionActive : classes.iconActions}/> :
                    <e.icon className={active == e.id ? classes.imageIconActive : classes.image}/>}
                <span className={active == e.id ? classes.titleActive : classes.title}>
                    {e.title}
                </span>
            </Link>)}
        </div>
    )
}

export default Navigation;
