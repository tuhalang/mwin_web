import React, {useEffect, useState} from "react";
import OTPInput from "./OTPInput";
import consts, {PRIMARY_COLOR} from "../../consts";
import {useTranslation} from "react-i18next";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {isMobile} from "react-device-detect";
import _ from 'lodash';

function OTPModal(props) {

    const {
        visible,
        handleOk,
        handleCancel,
        otpError,
        title,
        autoFocus,
        reSend,
        defaultValue
    } = props;
    const [loading, setLoading] = useState(false);
    const [timer, setTimer] = useState(0);
    const [count, setCount] = useState(60);
    const [isReSend, setIsResend] = useState(false);
    const {t} = useTranslation('translation')

    const handlerOTP = (otp) => {
        handleOk(otp)
        // utils.showNotification('OTP code', 'Send OTP Fail', 'error')
    }

    const countDown = () => {
        setCount(60);
    }

    useEffect(() => {
        if (!count) {
            setIsResend(true)
            setCount(60)
        }
        const intervalId = setInterval(() => {
            if (!isReSend) setCount(count - 1);
        }, 1000);

        return () => clearInterval(intervalId);
    }, [count])


    return (
        <div style={{
            width: '100%',
            display: visible ? 'flex' : 'none',
            position: 'fixed',
            bottom: 0,
            left: 0,
            zIndex: 2000,
            height: '100%',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'flex-end',
            backgroundColor: 'rgba(0,0,0,0.3)',
            '-webkit-transform': 'translateZ(0)'
        }} onClick={e => {
            e.preventDefault();
            if (e.target === e.currentTarget) {
                handleCancel()
            }
        }}>
            <div style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                backgroundColor: 'white',
                width: isMobile ? '100vw' : 400,
                borderRadius: '20px 20px 0px 0px'
            }}>
                <div style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '100%',
                    padding: '13px 20px 13px 20px',
                }}>
                    <div style={{width: 14, height: 14}}></div>
                    <span style={{
                        color: '#181725',
                        fontWeight: 600,
                        fontSize: 18,
                        letterSpacing: 0.1,
                    }}>{title ? title : t('label-confirm-otp')}</span>
                    <FontAwesomeIcon icon={faTimes} size={14} style={{width: 14, height: 14, overflow: 'hidden'}}
                                     onClick={handleCancel}/>
                </div>
                <span style={{
                    fontSize: 13,
                    fontWeight: 500,
                    lineHeight: '17px',
                    marginBottom: 20,
                }}>{t('otp-request-label')}</span>
                <OTPInput
                    autoFocus={autoFocus || true}
                    isNumberInput
                    length={4}
                    defaultValue={defaultValue}
                    className="otpContainer"
                    inputClassName="otpInput"
                    onChangeOTP={(otp) => {
                        if (otp.length == 4) {
                            handlerOTP(otp)
                        }
                    }}
                />
                <span style={{fontSize: 12, color: 'red', height: 18}}>{otpError}</span>
                {isReSend ? <div onClick={() => setIsResend(false)} style={{
                    fontSize: 13,
                    fontWeight: 400,
                    lineHeight: '17px',
                    marginTop: 20,
                    marginBottom: 20,
                }}>{t('get-otp')}</div> : <span style={{
                    fontSize: 13,
                    fontWeight: 400,
                    lineHeight: '17px',
                    marginTop: 20,
                    marginBottom: 20,
                }}>{t('send-otp-back')} <span style={{color: PRIMARY_COLOR}}>{count}s</span></span>}
            </div>
            {/*</Modal>*/}
        </div>
    )
}

export default OTPModal;
