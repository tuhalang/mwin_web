import React, {useEffect, useState} from "react";
import {Button, Modal} from "antd";
import styles from 'assets/jss/modal'
import consts, {PRIMARY_COLOR} from "../../consts";
import {makeStyles} from "@material-ui/core/styles";
import 'assets/css/modal_repair.css';
import {useTranslation} from "react-i18next";
import OtpItem from "../FormItem/OtpItem";

const useStyles = makeStyles(styles);

function OTPModalV2(props) {
    const classes = useStyles();
    const {
        visible,
        handleOk,
        handleCancel,
        otpError,
        title,
        reSend,
    } = props;
    const [loading, setLoading] = useState(false);
    const {t, i18n} = useTranslation('translation');
    const [timeOTP, setTimeOTP] = useState(3);
    const [otp, setOtp] = useState('');
    const [defaultValue, setDefaultValue] = useState('');

    const getOtpFromSMS = () => {
        if (!window.OTPCredential) {
            console.log('browser not support')
            return;
        }
        window.addEventListener('DOMContentLoaded', e => {
            const abort = new AbortController();

            setTimeout(() => {
                // abort after two minutes
                abort.abort();
            }, 2 * 60 * 1000);
            navigator.credentials.get({
                signal: abort.signal,
                otp: {
                    transport: ["sms"]
                }
            }).then(({code, type}) => {
                if (code) {
                    setDefaultValue(code)
                }
                abort.abort();
            });
        })
    }

    useEffect(() => {
        getOtpFromSMS()
    }, [])

    return (
        <>
            <Modal title=""
                   destroyOnClose={true}
                   visible={visible}
                   onOk={() => handleOk(otp)}
                   onCancel={handleCancel}
                   width={300}
                   closable={false}
                   bodyStyle={{borderRadius: 10}}
                   footer={[
                       <div className={classes.formItem}>
                           <OtpItem label={'Otp'} require={true} placeholder={t('otp-modal-placeholder')}
                                    errorMessage={otpError} name={'otpInput'}
                                    autoComplete={'one-time-code'}
                                    defaultValue={defaultValue}
                                    reSend={reSend}/>
                           <Button style={{backgroundColor: PRIMARY_COLOR}} className={classes.btnModal}
                                   type={'submit'}
                                   loading={loading}
                                   onClick={() => handleOk(otp)}>{t('label-confirm-otp')}</Button>
                       </div>
                   ]}
            >
                <div style={{textAlign: 'center',}}>
                <span style={{
                    fontSize: 13,
                    lineHeight: '18px',
                    color: '#181725',
                    fontWeight: 600
                }}>{title || ''}</span>
                </div>
            </Modal>
        </>
    )
}

export default OTPModalV2;
