import React, {memo, useRef, useLayoutEffect} from "react";
import {usePrevious} from "react-use";
import {Input} from "antd";
import styles from 'assets/jss/OTP/OTPInput'
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(styles);

export function SingleOTPInputComponent(props) {
    const {focus, autoFocus, ...rest} = props;
    const classes = useStyles();
    const inputRef = useRef(null);
    const prevFocus = usePrevious(!!focus);
    useLayoutEffect(() => {
        if (inputRef.current) {
            if (focus && autoFocus) {
                inputRef.current.focus();
            }
            if (focus && autoFocus && focus !== prevFocus) {
                inputRef.current.focus();
                inputRef.current.select();
            }
        }
    }, [autoFocus, focus, prevFocus]);

    return <Input ref={inputRef} {...rest} className={classes.inputNumber}/>;
}

const SingleOTPInput = memo(SingleOTPInputComponent);
export default SingleOTPInput;
