export const BASE_URL = process.env.REACT_APP_SERVER_URL;
export const DNS_IMAGE = process.env.REACT_APP_DNS_IMAGE;
export const PREFIX_FRONT_URL = '';
export const CONTEXT_PATH = '/api';
export const TIME_OF_DEBOUNCE = 300;
export const PRIMARY_COLOR = '#FD7E14';
export const BACKGROUND_COLOR = 'white';
export const SECOND_COLOR = '#E6E6FA';
export const CURRENCY_UNIT = 'MT';

const CONTROLS = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
};

export default {
    // ============ RBAC ============
    // todo define all controls in screen here...
    CONTROLS,
    EN: 'en',
    VI: 'vi',
    KO: 'korea',
    TYPE_ERROR: 'error',
    PAGE_SIZE: 10,
    GAME_CODE: process.env.REACT_APP_GAME_CODE,
    OTP_LENGTH: process.env.REACT_APP_OTP_LENGTH || 4,
};
