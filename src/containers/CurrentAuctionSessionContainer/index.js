import {connect} from 'react-redux';
import actions from '../../redux/actions/session'
import actionsInfo from '../../redux/actions/info'
import Home from "../../views/Home/Home";

const mapStateToProps = (state) => ({
    sessions: state.session.sessions,
    player: state.session.player,
    images: state.session.images
});

const mapDispatchToProps = (dispatch) => ({
    onFetchCurrentAuctionSession: (params, callback) => {
        dispatch(actions.fetchCurrentAuctionSession(params, callback))
    },
    onPlayTurn: (params, callback) => {
        dispatch(actions.playTurn(params, callback))
    },
    onAddTurn: (params, callback) => {
        dispatch(actions.addTurn(params, callback))
    },
    registerService: (params, callback) => {
        dispatch(actions.registerService(params, callback))
    },
    onGetListPlayingOfCurrentSession: (params, callback) => {
        dispatch(actions.getListPlayingOfCurrentSession(params, callback))
    },
    openModalLogin: () => {
        dispatch(actionsInfo.openModalLogin())
    },
    getBanner: (params, callback) => {
        dispatch(actions.getBanner(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
