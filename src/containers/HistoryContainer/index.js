import {connect} from 'react-redux';
import actions from '../../redux/actions/history'
import HistoryPage from "../../views/HistoryPage/HistoryPage";

const mapStateToProps = (state) => ({
    histories: state.history.histories,
});

const mapDispatchToProps = (dispatch) => ({
    onFetchPaginateHistory: (params, callback) => {
        dispatch(actions.fetchPaginateHistory(params, callback))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryPage);
