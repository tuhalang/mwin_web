import {connect} from 'react-redux';
import actions from '../../redux/actions/info'
import InfoPage from "../../views/InfoPage/InfoPage";

const mapStateToProps = (state) => ({
    contactUs: state.info.contactUs,
    rule: state.info.rule,
});

const mapDispatchToProps = (dispatch) => ({
    cancelMwinService: (params, callback) => {
        dispatch(actions.cancelMwinService(params, callback))
    },
    getGuide: (params, callback) => {
        dispatch(actions.getGuide(params, callback))
    },
    getContactInfo: (params, callback) => {
        dispatch(actions.getContactInfo(params, callback))
    },
    logout: () => {
        dispatch(actions.logout())
    },
    login: () => {
        dispatch(actions.openModalLogin())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoPage);
