import {connect} from 'react-redux';
import ProfilePage from "../../views/ProfilePage/ProfilePage";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
