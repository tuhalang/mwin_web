import {connect} from 'react-redux';
import actions from '../../redux/actions/result'
import ResultPage from "../../views/ResultPage";

const mapStateToProps = (state) => ({
    sessions: state.result.sessions,
    result: state.result.result
});

const mapDispatchToProps = (dispatch) => ({
    onFetchCurrentAuctionSessionResult: (params, callback) => {
        dispatch(actions.fetchCurrentAuctionSessionResult(params, callback))
    },
    onFetchExpiredAuctionSessionResult: (params, callback) => {
        dispatch(actions.fetchExpiredAuctionSessionResult(params, callback))
    },
    onFetchListWinnerPrize: (params, callback) => {
        dispatch(actions.fetchListWinnerPrize(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ResultPage);
