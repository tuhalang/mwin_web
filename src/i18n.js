import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import translationEn from './locales/en/translation.json';
import translationVi from './locales/vi/translation.json';
import translationPt from './locales/pt/translation.json';
import historyEn from './locales/en/history.json';
import historyVi from './locales/vi/history.json';
import historyPt from './locales/pt/history.json';
import resultEn from './locales/en/result.json';
import resultVi from './locales/vi/result.json';
import resultPt from './locales/pt/result.json';
import infoEn from './locales/en/info.json';
import infoVi from './locales/vi/info.json';
import infoPt from './locales/pt/info.json';
import playEn from './locales/en/play.json';
import playVi from './locales/vi/play.json';
import playPt from './locales/pt/play.json';

const resources = {
    en: {
        translation: translationEn,
        history: historyEn,
        result: resultEn,
        info: infoEn,
        play: playEn,
    },
    vi: {
        translation: translationVi,
        history: historyVi,
        result: resultVi,
        info: infoVi,
        play: playVi,
    },
    pt: {
        translation: translationPt,
        history: historyPt,
        result: resultPt,
        info: infoPt,
        play: playPt,
    }
};

i18n
    // load translation using http -> see /public/locales (i.e. https://github.com/i18next/react-i18next/tree/master/example/react/public/locales)
    // learn more: https://github.com/i18next/i18next-http-backend
    .use(Backend)
    // detect user language
    // learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(LanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        resources,
        fallbackLng: 'pt',
        lng: 'pt',
        debug: true,
        ns: ['translation', 'history', 'result', 'info', 'play'],
        defaultNS: 'translation',
        interpolation: {
            escapeValue: false,
        },
        react: {
            useSuspense: false,
        }
    });


export default i18n;
