import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import './env';
import {createBrowserHistory} from "history";
import {Route, Switch, HashRouter} from "react-router-dom";
import 'antd/dist/antd.css';
import createSagaMiddleware from 'redux-saga';
import "assets/scss/material-kit-react.scss?v=1.9.0";
import Home from "containers/CurrentAuctionSessionContainer";
import './i18n'
import {BASE_URL} from "./consts";
import {applyMiddleware, compose, createStore} from "redux";
import allReducers from "./redux/reducers";
import rootSaga from "./redux/sagas/root_saga";
import axios from "axios";
import Loading from "./components/Loading";
import HistoryPage from "./containers/HistoryContainer";
import InfoPage from "containers/InfoContainer";
import {Provider} from "react-redux";
import {Redirect} from "react-router";
import ResultPage from "./containers/ResultContainer";
import App from "./app";

window.axios = axios.create({
    baseURL: BASE_URL,
});

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const store = createStore(
    allReducers,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

window.$dispatch = store.dispatch;
store.dispatch({
    type: '@@__INIT__',
});

ReactDOM.render(
    <Provider store={store}>
        {/*<HashRouter history={hist}>*/}
        {/*    <Suspense fallback={<Loading/>}>*/}
        {/*        <Switch>*/}
        {/*            <Route path={"/history"} component={HistoryPage}/>*/}
        {/*            <Route path={"/info"} component={InfoPage}/>*/}
        {/*            <Route path={"/result"} component={ResultPage}/>*/}
        {/*            <Route path={"/play"} component={Home}/>*/}
        {/*            <Redirect from="/" to={"/play"}/>*/}
        {/*            /!*<Redirect from="/play" to={"/play"}/>*!/*/}
        {/*        </Switch>*/}
        {/*    </Suspense>*/}
        {/*</HashRouter>*/}
        <App/>
    </Provider>,
    document.getElementById("root")
);
