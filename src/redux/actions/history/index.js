import {FETCH_PAGINATE_HISTORY, FETCH_PAGINATE_HISTORY_FAILED, FETCH_PAGINATE_HISTORY_SUCCEED} from "./action_types";

export default {
    fetchPaginateHistory: (params, callback) => ({
        type: FETCH_PAGINATE_HISTORY,
        params,
        callback,
    }),
    fetchPaginateHistorySucceed: (data) => ({
        type: FETCH_PAGINATE_HISTORY_SUCCEED,
        data,
    }),
    fetchPaginateHistoryFailed: (err) => ({
        type: FETCH_PAGINATE_HISTORY_FAILED,
        err,
    }),
}
