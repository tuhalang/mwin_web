import {
    CANCEL_MODAL_LOGIN, CANCEL_MODAL_LOGIN_FAILED, CANCEL_MODAL_LOGIN_SUCCEED,
    CANCEL_MWIN_SERVICE,
    CANCEL_MWIN_SERVICE_FAILED,
    CANCEL_MWIN_SERVICE_SUCCEED,
    CONTACT_INFO,
    CONTACT_INFO_FAILED,
    CONTACT_INFO_SUCCEED,
    GET_GUIDE,
    GET_GUIDE_FAILED,
    GET_GUIDE_SUCCEED,
    GET_OTP,
    GET_OTP_FAILED,
    GET_OTP_SUCCEED,
    LOGIN,
    LOGIN_FAILED,
    LOGIN_SUCCEED, LOGOUT, LOGOUT_FAILED, LOGOUT_SUCCEED,
    OPEN_MODAL_LOGIN, OPEN_MODAL_LOGIN_FAILED,
    OPEN_MODAL_LOGIN_SUCCEED
} from "./action_types";


export default {
    cancelMwinService: (params, callback) => ({
        type: CANCEL_MWIN_SERVICE,
        params,
        callback,
    }),
    cancelMwinServiceSucceed: () => ({
        type: CANCEL_MWIN_SERVICE_SUCCEED,
    }),
    cancelMwinServiceFailed: (err) => ({
        type: CANCEL_MWIN_SERVICE_FAILED,
        err,
    }),

    getGuide: (params, callback) => ({
        type: GET_GUIDE,
        params,
        callback,
    }),
    getGuideSucceed: (data) => ({
        type: GET_GUIDE_SUCCEED,
        data,
    }),
    getGuideFailed: (err) => ({
        type: GET_GUIDE_FAILED,
        err,
    }),

    getContactInfo: (params, callback) => ({
        type: CONTACT_INFO,
        params,
        callback,
    }),
    getContactInfoSucceed: (data) => ({
        type: CONTACT_INFO_SUCCEED,
        data,
    }),
    getContactInfoFailed: (err) => ({
        type: CONTACT_INFO_FAILED,
        err,
    }),

    login: (params, callback) => ({
        type: LOGIN,
        params,
        callback,
    }),
    loginSucceed: (token, isdn) => ({
        type: LOGIN_SUCCEED,
        token,
        isdn,
    }),
    loginFailed: (err) => ({
        type: LOGIN_FAILED,
        err,
    }),

    getOtp: (params, callback) => ({
        type: GET_OTP,
        params,
        callback,
    }),
    getOtpSucceed: (data) => ({
        type: GET_OTP_SUCCEED,
        data,
    }),
    getOtpFailed: (err) => ({
        type: GET_OTP_FAILED,
        err,
    }),

    openModalLogin: () => ({
        type: OPEN_MODAL_LOGIN,
    }),
    openModalLoginSucceed: (data) => ({
        type: OPEN_MODAL_LOGIN_SUCCEED,
        data,
    }),
    openModalLoginFailed: (err) => ({
        type: OPEN_MODAL_LOGIN_FAILED,
        err,
    }),

    cancelModalLogin: () => ({
        type: CANCEL_MODAL_LOGIN,
    }),
    cancelModalLoginSucceed: (data) => ({
        type: CANCEL_MODAL_LOGIN_SUCCEED,
    }),
    cancelModalLoginFailed: (err) => ({
        type: CANCEL_MODAL_LOGIN_FAILED,
        err,
    }),

    logout: () => ({
        type: LOGOUT,
    }),
    logoutSucceed: (data) => ({
        type: LOGOUT_SUCCEED,
    }),
    logoutFailed: (err) => ({
        type: LOGOUT_FAILED,
        err,
    }),
}
