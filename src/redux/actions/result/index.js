import {
    FETCH_CURRENT_AUCTION_SESSION_RESULT,
    FETCH_CURRENT_AUCTION_SESSION_RESULT_FAILED,
    FETCH_CURRENT_AUCTION_SESSION_RESULT_SUCCEED,
    FETCH_EXPIRED_AUCTION_SESSION_RESULT,
    FETCH_EXPIRED_AUCTION_SESSION_RESULT_FAILED,
    FETCH_EXPIRED_AUCTION_SESSION_RESULT_SUCCEED,
    FETCH_LIST_WINNER_PRIZE,
    FETCH_LIST_WINNER_PRIZE_FAILED,
    FETCH_LIST_WINNER_PRIZE_SUCCEED
} from "./action_types";

export default {
    fetchCurrentAuctionSessionResult: (params, callback) => ({
        type: FETCH_CURRENT_AUCTION_SESSION_RESULT,
        params,
        callback,
    }),
    fetchCurrentAuctionSessionResultSucceed: (data) => ({
        type: FETCH_CURRENT_AUCTION_SESSION_RESULT_SUCCEED,
        data,
    }),
    fetchCurrentAuctionSessionResultFailed: (err) => ({
        type: FETCH_CURRENT_AUCTION_SESSION_RESULT_FAILED,
        err,
    }),

    fetchExpiredAuctionSessionResult: (params, callback) => ({
        type: FETCH_EXPIRED_AUCTION_SESSION_RESULT,
        params,
        callback,
    }),
    fetchExpiredAuctionSessionResultSucceed: (data) => ({
        type: FETCH_EXPIRED_AUCTION_SESSION_RESULT_SUCCEED,
        data,
    }),
    fetchExpiredAuctionSessionResultFailed: (err) => ({
        type: FETCH_EXPIRED_AUCTION_SESSION_RESULT_FAILED,
        err,
    }),

    fetchListWinnerPrize: (params, callback) => ({
        type: FETCH_LIST_WINNER_PRIZE,
        params,
        callback,
    }),
    fetchListWinnerPrizeSucceed: (data) => ({
        type: FETCH_LIST_WINNER_PRIZE_SUCCEED,
        data,
    }),
    fetchListWinnerPrizeFailed: (err) => ({
        type: FETCH_LIST_WINNER_PRIZE_FAILED,
        err,
    }),
}
