import {
    AUCTION_ADD_TURN,
    AUCTION_ADD_TURN_FAILED,
    AUCTION_ADD_TURN_SUCCEED,
    AUCTION_PLAY_TURN,
    AUCTION_PLAY_TURN_FAILED,
    AUCTION_PLAY_TURN_SUCCEED, EXPIRE_AUCTION_SESSION, EXPIRE_AUCTION_SESSION_FAILED, EXPIRE_AUCTION_SESSION_SUCCEED,
    FETCH_CURRENT_AUCTION_SESSION,
    FETCH_CURRENT_AUCTION_SESSION_FAILED,
    FETCH_CURRENT_AUCTION_SESSION_SUCCEED, GET_BANNER, GET_BANNER_FAILED, GET_BANNER_SUCCEED,
    LIST_PLAYING_CURRENT_SESSION,
    LIST_PLAYING_CURRENT_SESSION_FAILED,
    LIST_PLAYING_CURRENT_SESSION_SUCCEED, REGISTER_SERVICE, REGISTER_SERVICE_FAILED, REGISTER_SERVICE_SUCCEED,
    UPCOMING_AUCTION_SESSION,
    UPCOMING_AUCTION_SESSION_FAILED,
    UPCOMING_AUCTION_SESSION_SUCCEED
} from "./action_types";

export default {
    fetchCurrentAuctionSession: (params, callback) => ({
        type: FETCH_CURRENT_AUCTION_SESSION,
        params,
        callback,
    }),
    fetchCurrentAuctionSessionSucceed: (data) => ({
        type: FETCH_CURRENT_AUCTION_SESSION_SUCCEED,
        data,
    }),
    fetchCurrentAuctionSessionFailed: (err) => ({
        type: FETCH_CURRENT_AUCTION_SESSION_FAILED,
        err,
    }),

    playTurn: (params, callback) => ({
        type: AUCTION_PLAY_TURN,
        params,
        callback,
    }),
    playTurnSucceed: () => ({
        type: AUCTION_PLAY_TURN_SUCCEED,
    }),
    playTurnFailed: (err) => ({
        type: AUCTION_PLAY_TURN_FAILED,
        err,
    }),

    addTurn: (params, callback) => ({
        type: AUCTION_ADD_TURN,
        params,
        callback,
    }),
    addTurnSucceed: () => ({
        type: AUCTION_ADD_TURN_SUCCEED,
    }),
    addTurnFailed: (err) => ({
        type: AUCTION_ADD_TURN_FAILED,
        err,
    }),

    getListPlayingOfCurrentSession: (params, callback) => ({
        type: LIST_PLAYING_CURRENT_SESSION,
        params,
        callback,
    }),
    getListPlayingOfCurrentSessionSucceed: (data) => ({
        type: LIST_PLAYING_CURRENT_SESSION_SUCCEED,
        data,
    }),
    getListPlayingOfCurrentSessionFailed: (err) => ({
        type: LIST_PLAYING_CURRENT_SESSION_FAILED,
        err,
    }),

    getUpcomingAuctionSession: (params, callback) => ({
        type: UPCOMING_AUCTION_SESSION,
        params,
        callback,
    }),
    getUpcomingAuctionSessionSucceed: () => ({
        type: UPCOMING_AUCTION_SESSION_SUCCEED,
    }),
    getUpcomingAuctionSessionFailed: (err) => ({
        type: UPCOMING_AUCTION_SESSION_FAILED,
        err,
    }),

    getExpireAuctionSession: (params, callback) => ({
        type: EXPIRE_AUCTION_SESSION,
        params,
        callback,
    }),
    getExpireAuctionSessionSucceed: () => ({
        type: EXPIRE_AUCTION_SESSION_SUCCEED,
    }),
    getExpireAuctionSessionFailed: (err) => ({
        type: EXPIRE_AUCTION_SESSION_FAILED,
        err,
    }),

    registerService: (params, callback) => ({
        type: REGISTER_SERVICE,
        params,
        callback,
    }),
    registerServiceSucceed: () => ({
        type: REGISTER_SERVICE_SUCCEED,
    }),
    registerServiceFailed: (err) => ({
        type: REGISTER_SERVICE_FAILED,
        err,
    }),

    getBanner: (params, callback) => ({
        type: GET_BANNER,
        params,
        callback,
    }),
    getBannerSucceed: (images) => ({
        type: GET_BANNER_SUCCEED,
        images
    }),
    getBannerFailed: (err) => ({
        type: GET_BANNER_FAILED,
        err,
    }),
}
