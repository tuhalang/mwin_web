import {FETCH_PAGINATE_HISTORY_FAILED, FETCH_PAGINATE_HISTORY_SUCCEED} from "../actions/history/action_types";

const defaultParams = {
    histories: []
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_PAGINATE_HISTORY_SUCCEED:
            console.log(action.data, "data session")
            return {
                ...state,
                histories: action.data,
            }
        case FETCH_PAGINATE_HISTORY_FAILED:
            return {
                ...state,
                histories: []
            }
        default:
            return {
                ...state,
            };
    }
}
