import {combineReducers} from 'redux';
import history from './history_reducer'
import session from './session_reducer'
import info from './info_reducer';
import result from './result_reducer';

const allReducers = combineReducers({
    history,
    session,
    info,
    result,
});

export default allReducers;
