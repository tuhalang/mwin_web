import {
    CANCEL_MODAL_LOGIN,
    CONTACT_INFO_SUCCEED,
    GET_GUIDE_SUCCEED,
    LOGIN_SUCCEED, LOGOUT, LOGOUT_SUCCEED,
    OPEN_MODAL_LOGIN
} from "../actions/info/action_types";
import axios from "axios";
import {BASE_URL} from "../../consts";
import _ from 'lodash';

const defaultParams = {
    contactUs: [],
    rule: '',
    openModalLogin: false,
    isLogout: false,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case CONTACT_INFO_SUCCEED:
            return {
                ...state,
                contactUs: action.data,
            }
        case GET_GUIDE_SUCCEED:
            return {
                ...state,
                rule: action.data,
            }
        case LOGIN_SUCCEED:
            console.log(action, "login success")
            localStorage.setItem('token', action.token)
            localStorage.setItem('isdn', action.isdn)
            return {
                ...state,
                openModalLogin: false,
            }
        case OPEN_MODAL_LOGIN:
            return {
                ...state,
                openModalLogin: true,
            }
        case CANCEL_MODAL_LOGIN:
            window.location.replace('/')
            _.debounce(() => {
                return {
                    ...state,
                    openModalLogin: false,
                }
            }, 100);
            break;

        case LOGOUT:
            return {
                ...state,
                isLogout: true
            }
        case LOGOUT_SUCCEED:
            localStorage.removeItem('token')
            localStorage.removeItem('isdn')
            window.axios = axios.create({
                baseURL: BASE_URL,
            });
            return {
                ...state,
                isLogout: false
            }
        default:
            return {
                ...state,
            };
    }
}
