import {
    FETCH_CURRENT_AUCTION_SESSION_RESULT_SUCCEED,
    FETCH_EXPIRED_AUCTION_SESSION_RESULT_SUCCEED,
    FETCH_LIST_WINNER_PRIZE,
    FETCH_LIST_WINNER_PRIZE_SUCCEED
} from "../actions/result/action_types";

const defaultParams = {
    sessions: [],
    result: []
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_CURRENT_AUCTION_SESSION_RESULT_SUCCEED:
            console.log(action.data, "data session")
            return {
                ...state,
                sessions: action.data,
            }
        case FETCH_EXPIRED_AUCTION_SESSION_RESULT_SUCCEED:
            console.log(action.data, "data session")
            return {
                ...state,
                sessions: action.data,
            }
        case FETCH_LIST_WINNER_PRIZE_SUCCEED:
            return {
                ...state,
                result: action.data,
            }
        default:
            return {
                ...state,
            };
    }
}
