import {
    FETCH_CURRENT_AUCTION_SESSION_SUCCEED,
    GET_BANNER_SUCCEED,
    LIST_PLAYING_CURRENT_SESSION_FAILED,
    LIST_PLAYING_CURRENT_SESSION_SUCCEED
} from "../actions/session/action_types";
import _ from 'lodash';
import utils from "../../common/utils";

const defaultParams = {
    sessions: [],
    player: [],
    images: [],
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_CURRENT_AUCTION_SESSION_SUCCEED:
            console.log(action.data, "data session")
            return {
                ...state,
                sessions: action.data,
            }
        case LIST_PLAYING_CURRENT_SESSION_SUCCEED:
            return {
                ...state,
                player: action.data,
            }
        case LIST_PLAYING_CURRENT_SESSION_FAILED:
            return {
                ...state,
                player: []
            }
        // case AUCTION_PLAY_TURN_SUCCEED:
        //     return {
        //         ...state,
        //     }
        case GET_BANNER_SUCCEED:
            // console.log(action.images, "action.images")
            // let imageTmp = _.map(action.images, i => {
            //     const img = new Image();
            //     img.src = i.imageUrl;
            //     img.alt = i.imageTitle;
            //     return img
            // })
            return {
                ...state,
                images: action.images,
            }
        default:
            return {
                ...state,
            };
    }
}
