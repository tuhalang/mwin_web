import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import {FETCH_PAGINATE_HISTORY} from "../actions/history/action_types";
import rf from "../../requests/RequestFactory";
import _ from "lodash";
import consts from "../../consts";
import actions from "../actions/history";
import auth from '../actions/info';
import utils from "../../common/utils";
import actionsInfo from '../actions/info';
import i18n from '../../i18n';

const {t} = i18n

function* fetchPaginateHistory(action) {
    console.log('=== fetchPaginateHistory ===', action);
    let token = localStorage.getItem('token');
    if (!token || token == '') {
        auth.openModalLogin()
        return;
    }
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('HistoryRequest').fetchPaginateHistory(params), action.params);
        console.log(data, 'data')
        if (data.errorCode != 'S200') {
            utils.showNotification(t('history-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.fetchPaginateHistoryFailed(new Error('Error Step 1')));
            if (_.isFunction(action.callback)) action.callback(false)
            return;
        }

        if (data.result.errorCode == 10) {
            localStorage.removeItem('token')
            localStorage.removeItem('isdn')
            utils.showNotification(t('history-req'), data.result.message, consts.TYPE_ERROR);
            // yield put(actions.getOtpFailed(new Error('Error Step 1')));
            yield put(actionsInfo.openModalLogin())
            return;
        }

        if (data.result.errorCode != '0') {
            utils.showNotification(t('history-req'), data.result.message, consts.TYPE_ERROR);
            yield put(actions.getOtpFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchPaginateHistorySucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback(true)
    } catch (err) {
        yield put(actions.fetchPaginateHistoryFailed(err));
        if (_.isFunction(action.callback)) action.callback(false);
    }
}

function* watchAllHistories() {
    yield takeLatest(FETCH_PAGINATE_HISTORY, fetchPaginateHistory);
}

export default function* rootSaga() {
    yield all([fork(watchAllHistories)]);
}
