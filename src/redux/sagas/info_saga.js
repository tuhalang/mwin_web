import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts, {BASE_URL} from "../../consts";
import actions from "../actions/info";
import _ from "lodash";
import axios from "axios";
import {CANCEL_MWIN_SERVICE, CONTACT_INFO, GET_GUIDE, GET_OTP, LOGIN} from "../actions/info/action_types";
import i18n from '../../i18n';

const {t} = i18n

function* cancelMwinService(action) {
    console.log('=== cancelMwinService ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('InfoRequest').cancelMwinService(params), action.params);
        console.log(data, 'data cancelMwinService')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('cancel-auction-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.cancelMwinServiceFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.cancelMwinServiceSucceed());
        if (_.isFunction(action.callback)) action.callback(data.result)
    } catch (err) {
        yield put(actions.cancelMwinServiceFailed(err));
    }
}

function* getGuide(action) {
    console.log('=== getGuide ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('InfoRequest').getGuide(params), action.params);
        console.log(data, 'data getGuide')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('guide-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.getGuideFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.getGuideSucceed(data.result.wsResponse.rule || ''));
        if (_.isFunction(action.callback)) action.callback(data.result.wsResponse.rule || '')
    } catch (err) {
        yield put(actions.getGuideFailed(err));
    }
}


function* getContactUs(action) {
    console.log('=== getContactUs ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('InfoRequest').getContactUs(params), action.params);
        console.log(data, 'data getContactUs')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('contact-us-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.getContactInfoFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.getContactInfoSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.getContactInfoFailed(err));
    }
}

function* login(action) {
    console.log('=== login ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('InfoRequest').login(params), action.params);
        console.log(data, 'data login')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('login-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.loginFailed(new Error('Error Step 1')));
            return;
        }
        if (data.result.errorCode != '0') {
            utils.showNotification(t('otp-req'), data.result.message, consts.TYPE_ERROR);
            yield put(actions.getOtpFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.loginSucceed(data.result.message, action.params.isdn));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.loginFailed(err));
    }
}

function* getOtp(action) {
    console.log('=== getOtp ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('InfoRequest').getOtp(params), action.params);
        console.log(data, 'data getOtp')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('otp-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.getOtpFailed(new Error('Error Step 1')));
            return;
        }
        if (data.result.errorCode != '0') {
            utils.showNotification(t('otp-req'), data.result.message, consts.TYPE_ERROR);
            yield put(actions.getOtpFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.getOtpSucceed(data.result.message));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.getOtpFailed(err));
    }
}


function* watchAllInfo() {
    yield takeLatest(CANCEL_MWIN_SERVICE, cancelMwinService);
    yield takeLatest(GET_GUIDE, getGuide);
    yield takeLatest(CONTACT_INFO, getContactUs);
    yield takeLatest(LOGIN, login);
    yield takeLatest(GET_OTP, getOtp)
}

export default function* rootSaga() {
    yield all([fork(watchAllInfo)]);
}
