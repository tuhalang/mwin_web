import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import {
    FETCH_CURRENT_AUCTION_SESSION_RESULT, FETCH_EXPIRED_AUCTION_SESSION_RESULT, FETCH_LIST_WINNER_PRIZE,
} from "../actions/result/action_types";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts from "../../consts";
import actions from "../actions/result";
import _ from "lodash";
import i18n from '../../i18n';

const {t} = i18n

function* fetchCurrentAuctionSessionResult(action) {
    console.log('=== fetchCurrentAuctionSessionResult ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').fetchCurrentAuctionSession(params), action.params);
        console.log(data, 'data')
        if (data.errorCode != 'S200') {
            utils.showNotification(t('curr-session-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.fetchCurrentAuctionSessionResultFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchCurrentAuctionSessionResultSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.fetchCurrentAuctionSessionResultFailed(err));
    }
}

function* fetchExpiredAuctionSessionResult(action) {
    console.log('=== fetchExpiredAuctionSessionResult ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').fetchExpiredAuctionSession(params), action.params);
        console.log(data, 'data')
        if (data.errorCode != 'S200') {
            utils.showNotification(t('expired-session-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.fetchExpiredAuctionSessionResultFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchExpiredAuctionSessionResultSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback(data.result.message);
    } catch (err) {
        yield put(actions.fetchExpiredAuctionSessionResultFailed(err));
    }
}

function* fetchListWinnerPrize(action) {
    console.log('=== fetchListWinnerPrize ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('ResultRequest').fetchListWinnerPrize(params), action.params);
        console.log(data, 'data')
        if (data.errorCode != 'S200') {
            utils.showNotification(t('winner-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.fetchListWinnerPrizeFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchListWinnerPrizeSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback(data.result.wsResponse)
    } catch (err) {
        yield put(actions.fetchListWinnerPrizeFailed(err));
    }
}


function* watchAllResult() {
    yield takeLatest(FETCH_CURRENT_AUCTION_SESSION_RESULT, fetchCurrentAuctionSessionResult);
    yield takeLatest(FETCH_EXPIRED_AUCTION_SESSION_RESULT, fetchExpiredAuctionSessionResult);
    yield takeLatest(FETCH_LIST_WINNER_PRIZE, fetchListWinnerPrize);
}

export default function* rootSaga() {
    yield all([fork(watchAllResult)]);
}
