import {all} from 'redux-saga/effects';
import watchAllHistories from './history_saga'
import watchAllSession from './session_saga';
import watchAllInfo from './info_saga';
import watchAllResult from './result_saga';

function* rootSaga() {
    yield all([
        watchAllSession(),
        watchAllHistories(),
        watchAllInfo(),
        watchAllResult(),
    ]);
}

export default rootSaga;
