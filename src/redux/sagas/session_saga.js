import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import {
    AUCTION_ADD_TURN,
    AUCTION_PLAY_TURN, EXPIRE_AUCTION_SESSION,
    FETCH_CURRENT_AUCTION_SESSION, GET_BANNER,
    LIST_PLAYING_CURRENT_SESSION, REGISTER_SERVICE, UPCOMING_AUCTION_SESSION
} from "../actions/session/action_types";
import rf from "../../requests/RequestFactory";
import _ from "lodash";
import consts from "../../consts";
import actions from "../actions/session";
import actionsInfo from '../actions/info'
import utils from "../../common/utils";
import i18n from '../../i18n';

const {t} = i18n

function* fetchCurrentAuctionSession(action) {
    console.log('=== fetchCurrentAuctionSession ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').fetchCurrentAuctionSession(params), action.params);
        console.log(data, 'data')
        if (data.errorCode != 'S200') {
            utils.showNotification(t('curr-session-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.fetchCurrentAuctionSessionFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchCurrentAuctionSessionSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.fetchCurrentAuctionSessionFailed(err));
    }
}

function* playTurn(action) {
    console.log('=== playTurn ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').playTurn(params), action.params);
        console.log(data, 'data play turn')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('play-turn-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.playTurnFailed(new Error('Error Step 1')));
            return;
        }

        if (data.result.errorCode == 10) {
            localStorage.setItem('token', '')
            localStorage.setItem('isdn', '')
            utils.showNotification(t('play-turn-req'), data.result.message, consts.TYPE_ERROR);
            yield put(actionsInfo.openModalLogin())
            return;
        }
        if (data.result.errorCode != 0) {
            utils.showNotification(t('play-turn-req'), data.result.message, consts.TYPE_ERROR);
            yield put(actions.playTurnFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.playTurnSucceed());
        if (_.isFunction(action.callback)) action.callback(data.result)
    } catch (err) {
        yield put(actions.playTurnFailed(err));
    }
}

function* addTurn(action) {
    console.log('=== addTurn ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').addTurn(params), action.params);
        console.log(data, 'data add turn')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('add-turn-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.addTurnFailed(new Error('Error Step 1')));
            if (_.isFunction(action.callback)) action.callback(null)
            return;
        }
        yield put(actions.addTurnSucceed());
        if (_.isFunction(action.callback)) action.callback(data.result)
    } catch (err) {
        yield put(actions.addTurnFailed(err));
        if (_.isFunction(action.callback)) action.callback(null)
    }
}

function* getListPlayingOfCurrentSession(action) {
    console.log('=== getListPlayingOfCurrentSession ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').getListPlayingOfCurrentSession(params), action.params);
        console.log(data, 'data add turn')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('playing-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.getListPlayingOfCurrentSessionFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.getListPlayingOfCurrentSessionSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.getListPlayingOfCurrentSessionFailed(err));
    }
}

function* getUpcomingAuctionSession(action) {
    console.log('=== getUpcomingAuctionSession ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').getUpcomingAuctionSession(params), action.params);
        console.log(data, 'data add turn')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('upcoming-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.getUpcomingAuctionSessionFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.getUpcomingAuctionSessionSucceed());
        if (_.isFunction(action.callback)) action.callback(data.result)
    } catch (err) {
        yield put(actions.getUpcomingAuctionSessionFailed(err));
    }
}

function* getExpireAuctionSession(action) {
    console.log('=== getUpcomingAuctionSession ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').getExpireAuctionSession(params), action.params);
        console.log(data, 'data add turn')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('expired-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.getExpireAuctionSessionFailed(new Error('Error Step 1')));
            return;
        }
        yield put(actions.getExpireAuctionSessionSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback(data.result)
    } catch (err) {
        yield put(actions.getExpireAuctionSessionFailed(err));
    }
}

function* registerService(action) {
    console.log('=== registerService ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').registerService(params), action.params);
        console.log(data, 'register service')

        if (data.errorCode != 'S200') {
            utils.showNotification(t('register-req'), data.errorMessage, consts.TYPE_ERROR);
            yield put(actions.registerServiceFailed(new Error('Error Step 1')));
            if (_.isFunction(action.callback)) action.callback(null)
            return;
        }
        yield put(actions.registerServiceSucceed());
        if (_.isFunction(action.callback)) action.callback(data.result)
    } catch (err) {
        yield put(actions.registerServiceFailed(err));
        if (_.isFunction(action.callback)) action.callback(null)
    }
}

function* getBanner(action) {
    console.log('=== getBanner ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').getBanner(params), action.params);
        console.log(data, 'getBanner')

        if (data.errorCode != 'S200') {
            yield put(actions.getBannerFailed(new Error('Error Step 1')));
            if (_.isFunction(action.callback)) action.callback(null)
            return;
        }
        yield put(actions.getBannerSucceed(data.result.wsResponse));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.getBannerFailed(err));
        if (_.isFunction(action.callback)) action.callback(null)
    }
}

function* watchAllSession() {
    yield takeLatest(FETCH_CURRENT_AUCTION_SESSION, fetchCurrentAuctionSession);
    yield takeLatest(AUCTION_PLAY_TURN, playTurn);
    yield takeLatest(AUCTION_ADD_TURN, addTurn);
    yield takeLatest(LIST_PLAYING_CURRENT_SESSION, getListPlayingOfCurrentSession);
    yield takeLatest(UPCOMING_AUCTION_SESSION, getUpcomingAuctionSession);
    yield takeLatest(EXPIRE_AUCTION_SESSION, getExpireAuctionSession);
    yield takeLatest(REGISTER_SERVICE, registerService);
    yield takeLatest(GET_BANNER, getBanner);
}

export default function* rootSaga() {
    yield all([fork(watchAllSession)]);
}
