import React from "react";
import BaseRequest from './BaseRequest';
import _ from "lodash";

/**
 * key base on host:port
 */
export default class HistoryRequest extends BaseRequest {

    /**
     * @param {string} params.isdn
     * @param {string} params.gameCode
     * @param {string} params.language
     * @param {string} params.pageNumber
     * @param {string} params.pageSize
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchPaginateHistory(params) {
        const url = ``;
        let token = localStorage.getItem('token')
        _.assign(params, {token})
        let data = {
            "isWrap": '1',
            "wsCode": 'wsGetAuctionHistory',
            "wsRequest": params,
        }
        return this.post(url, data);
    }
}
