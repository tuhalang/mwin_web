import React from "react";
import BaseRequest from './BaseRequest';

/**
 * key base on host:port
 */
export default class InfoRequest extends BaseRequest {

    /**
     * @param {string} params.isdn
     * @param {string} params.gameCode
     * @param {string} params.language
     * @param {string} params.otpType
     * @param {string} params.transid
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    cancelMwinService(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsCancelMwinService',
            "wsRequest": params,
        }
        return this.post(url, data);
    }


    /**
     * @param {string} params.isdn
     * @param {string} params.gameCode
     * @param {string} params.language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getGuide(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsMwinGetGuide',
            "wsRequest": params,
        }
        return this.post(url, data);
    }



    /**
     * @param {string} params.gameCode
     * @param {string} params.language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getContactUs(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsGetContactInfo',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} params.isdn
     * @param {string} params.otp
     * @param {string} params.gameCode
     * @param {string} params.language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    login(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsLoginOtp',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} params.isdn
     * @param {string} params.gameCode
     * @param {string} params.language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getOtp(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsGetOtp',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

}
