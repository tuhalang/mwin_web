import SessionRequest from './SessionRequest';
import HistoryRequest from "./HistoryRequest";
import InfoRequest from "./InfoRequest";
import ResultRequest from './ResultRequest';

const requestMap = {
    SessionRequest,
    HistoryRequest,
    InfoRequest,
    ResultRequest,
};

const instances = {};

export default class RequestFactory {
    static getRequest(classname) {
        const RequestClass = requestMap[classname];
        if (!RequestClass) {
            throw new Error(`Invalid request class name: ${classname}`);
        }

        let requestInstance = instances[classname];
        if (!requestInstance) {
            requestInstance = new RequestClass();
            instances[classname] = requestInstance;
        }
        return requestInstance;
    }
}
