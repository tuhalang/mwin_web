import React from "react";
import BaseRequest from './BaseRequest';

/**
 * key base on host:port
 */
export default class ResultRequest extends BaseRequest {
    /**
     * @param {string} params.auctionSessionId
     * @param {string} params.gameCode
     * @param {string} params.productId
     * @param {string} params.language
     * @param {string} params.pageNumber
     * @param {string} params.pageSize
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchListWinnerPrize(params) {
        const url = ``;
        let data = {
            "isWrap": "1",
            "wsCode": 'wsGetPrizeResult',
            "wsRequest": params,
        }
        return this.post(url, data);
    }
}
