import React from "react";
import BaseRequest from './BaseRequest';
import _ from 'lodash';

/**
 * key base on host:port
 */
export default class SessionRequest extends BaseRequest {

    /**
     * @param {string} params.isdn
     * @param {string} params.gameCode
     * @param {string} params.language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchCurrentAuctionSession(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsGetCurrentAuctionSession',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} params.isdn
     * @param {string} params.gameCode
     * @param {string} params.language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchExpiredAuctionSession(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsGetExpireAuctionSession',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} auctionSessionId
     * @param {string} productId
     * @param {string} price
     * @param {string} language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    playTurn(params) {
        let token = localStorage.getItem('token')
        _.assign(params, {token})
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsAuctionPlayTurn',
            "wsRequest": params,
        }
        console.log(data, 'request playturn data');
        return this.post(url, data);
    }

    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} otp
     * @param {string} transid
     * @param {string} otpType
     * @param {string} language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    addTurn(params) {
        const url = ``;
        let data = {
            "isWrap": "1",
            "wsCode": 'wsChargeMwinService',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} pageNumber
     * @param {string} pageSize
     * @param {string} auctionSessionId
     * @param {string} language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListPlayingOfCurrentSession(params) {
        const url = ``;
        let data = {
            "isWrap": '1',
            "wsCode": 'wsGetListPlayingOfCurrentSession',
            "wsRequest": params,
        }
        return this.post(url, data);
    }


    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getUpcomingAuctionSession(params) {
        const url = ``;
        let data = {
            "wsCode": 'wsGetUpcomingAuctionSession',
            "wsRequest": params,
        }
        return this.post(url, data);
    }


    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getExpireAuctionSession(params) {
        const url = ``;
        let data = {
            "wsCode": 'wsGetExpireAuctionSession',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} language
     * @param {string} otpType
     * @param {string} transid
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    registerService(params) {
        const url = ``;
        let data = {
            "isWrap": "1",
            "wsCode": 'wsRegisterMwinService',
            "wsRequest": params,
        }
        return this.post(url, data);
    }

    /**
     * @param {string} isdn
     * @param {string} gameCode
     * @param {string} language
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getBanner(params) {
        const url = ``;
        let data = {
            "isWrap": "1",
            "wsCode": 'wsGetListSlideShowImage',
            "wsRequest": params,
        }
        return this.post(url, data);
    }
}
