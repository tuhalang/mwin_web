import React, {useState} from "react";
import utils from "../../common/utils";
import {Button, Divider, Form} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/auction/auction";
import FormItem from "../../components/FormItem/FormItem";
import consts, {CURRENCY_UNIT, PRIMARY_COLOR} from "../../consts";
import OTPModal from "../../components/OTP/OTPModal";
import {useTranslation} from "react-i18next";
import Clock from "../../components/Clock/Clock";
import _ from 'lodash';
import AcceptAuctionModal from "../../components/Modal/AcceptAuction";
import RegisterServiceModal from "../../components/Modal/RegisterServiceModal";
import LinearLoading from "../../components/Loading/LinearLoading";
import AddTurnModal from "../../components/Modal/AddTurnModal";
import PriceItem from "../../components/FormItem/PriceItem";
import OTPModalV2 from "../../components/OTP/OTPModalV2";

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function AuctionPage(props) {
    const classes = useStyles();
    const {data, player} = props;
    const [errorPhone, setErrorPhone] = useState('')
    const [errorPrice, setErrorPrice] = useState('')
    const [otpAddTurn, setOtpAddTurn] = useState(false);
    const [otpError, setOTPError] = useState('');
    const [timeOTP, setTimeOTP] = useState(3);
    const [acceptModal, setAcceptModal] = useState(false);
    const [registerModal, setRegisterModal] = useState(false);
    const [addTurnModal, setAddTurnModal] = useState(false);
    const [otpRegister, setOtpRegister] = useState(false);
    const [price, setPrice] = useState(0);
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const {t, i18n} = useTranslation('play');

    const isdn = () => localStorage.getItem('isdn');


    const onFinish = (event) => {
        event.preventDefault();
        const {price} = event.target;
        if (price.value == '') setErrorPrice('Please input product price!')
        if (!utils.checkFloat(price.value)) {
            setErrorPrice('Number format error')
            return;
        }
        if (!utils.checkPrice(price.value)) {
            setErrorPrice('Number must be greater than 0')
            return;
        }
        //TODO
        //Xử lý form đấu giá
        setPrice(price.value)
        setAcceptModal(true)
    };


    const fetchListParticipant = (pageNumber = 1, pageSize = consts.PAGE_SIZE) => {
        props.onGetListPlayingOfCurrentSession({
            "isdn": isdn(),
            "gameCode": consts.GAME_CODE,
            "language": i18n.language,
            "pageNumber": pageNumber,
            "pageSize": consts.PAGE_SIZE,
            "auctionSessionId": data.auctionSessionId || ''
        }, () => {
            setLoading(false);
            setPage(pageNumber)
        })
    }

    const playTurn = () => {
        props.onPlayTurn({
            isdn: isdn(),
            gameCode: consts.GAME_CODE,
            auctionSessionId: data.auctionSessionId,  // Id của phiên
            productId: data.productId,   // Id của product khi chọn
            price,
            language: i18n.language
        }, (result) => {
            console.log(result.errorCode, 'errorCode')
            switch (result.errorCode) {
                case "2":
                    setRegisterModal(true);
                    break;
                case "5":
                    setOTPError('OTP over times')
                    utils.showNotification(t('notification'), t('otp-over-times'), 'warning')
                    break;
                case "6":
                    utils.showNotification(t('notification'), t('invalid-auction'), 'error')
                    break;
                case "7":
                    utils.showNotification(t('notification'), t('otp-expired'), 'error')
                    break;
                case "8":
                    utils.showNotification(t('notification'), t('otp-over-times-per-day'), 'error')
                    break;
                case "9":
                    utils.showNotification(t('notification'), t('otp-invalid-or-expired'), 'error')
                    setOTPError(t('otp-invalid-or-expired'))
                    break;
                case "0":
                    if (result.wsResponse.playType && result.wsResponse.playType == 'ADDTURN') {
                        setAddTurnModal(true);
                        break;
                    }
                    utils.showNotification(t('notification'), t('otp-success'), 'success');
                    fetchListParticipant();
                    break;
                default:
                    utils.showNotification(t('notification'), t('otp-success'), 'success');
                    fetchListParticipant();
                    break;
            }
        })
    }

    const registerService = (otp = '0') => {
        props.registerService({
            isdn: isdn(),
            "gameCode": consts.GAME_CODE,
            "language": i18n.language,
            "otpType": "1",
            "transid": data.auctionSessionId,
            "otp": otp,
        }, (resp) => {
            setLoading(false);
            if (resp && resp.errorCode == 0) {
                setOtpRegister(false);
                setAcceptModal(true);
                utils.showNotification(t('register-noti-title'), t('register-success'))
            } else {
                utils.showNotification(t('add-turn-noti-title'), resp.message || t('server-error'), 'warning')
            }
        })
    }

    const sendOtpRegisterService = () => {
        props.registerService({
            isdn: isdn(),
            "gameCode": consts.GAME_CODE,
            "language": i18n.language,
            "otpType": "0",
            "transid": data.auctionSessionId

        }, (resp) => {
            setLoading(false);
            if (resp && resp.errorCode == 0) {
                setOtpRegister(true);
            } else {
                utils.showNotification(t('register-noti-title'), resp.message || t('otp-register-fail'), 'error')
            }
        })
    }

    const addTurn = (otp = '0') => {
        props.onAddTurn({
            isdn: isdn(),
            "gameCode": consts.GAME_CODE,
            "language": i18n.language,
            otp,
            "otpType": "1",
            "transid": data.auctionSessionId,
        }, (resp) => {
            setLoading(false);
            if (resp && resp.errorCode == 0) {
                setOtpAddTurn(false);
                setAcceptModal(true);
            } else {
                utils.showNotification(t('add-turn-noti-title'), resp.message || t('server-error'), 'warning')
            }
        })
    }

    const sendOtpAddTurn = () => {
        props.onAddTurn({
            isdn: isdn(),
            "gameCode": consts.GAME_CODE,
            "language": i18n.language,
            "otpType": "0",
            "transid": data.auctionSessionId

        }, (resp) => {
            setLoading(false);
            if (resp && resp.errorCode == 0) {
                setOtpAddTurn(true);
            } else {
                utils.showNotification(t('add-turn-noti-title'), resp.message || t('server-error'), 'warning')
            }
        })
    }


    return (
        <div className={classes.container}>
            <div>
                <img src={utils.getImageUrl(data.imageUrl) || ''}
                     className={classes.image}/>
            </div>
            <div className={classes.content}>
                <div className={classes.infoProduct}>
                    <span className={classes.labelContent}>{t('name-product')}</span>
                    <span
                        className={classes.nameProduct}>{utils.ellipsisString(data.productName, 25)}</span>

                </div>
                <div className={classes.infoProduct}>
                    <span className={classes.labelContent}>{t('price')}</span>
                    <span className={classes.price}>{`${data.price} ${CURRENCY_UNIT}`}</span>
                </div>
                <div className={utils.checkLengthDesc(data.productDesc) ? classes.infoProduct : classes.desc2}>
                    <span className={classes.labelContent}>{t('desc')}</span>
                    <span className={classes.desc}>{data.productDesc}</span>
                </div>
            </div>
            <Clock time={utils.parseDateFromString(data.endDate)} style={{alignSelf: "flex-start"}}/>
            <form
                name="infoUser"
                onSubmit={onFinish}
                className={classes.formItem}
            >
                <PriceItem require={true} placeholder={t('price-placeholder')}
                           errorMessage={errorPrice} name={'price'} reset={() => setErrorPrice('')}/>
                <Button className={classes.btnAuction} type={'submit'}
                        htmlType="submit">{t('btn-label')} <FontAwesomeIcon
                    icon={faChevronRight} style={{marginLeft: 4,}}/></Button>
            </form>
            {player ? <div className={classes.listOfParticipants}>
                <div className={classes.listOfParticipants1}>
                    <span>{t('list-of-participant')}</span>
                    <span>{(page - 1) * consts.PAGE_SIZE + player.length}</span>
                </div>
                {_.map(player, e => {
                    return (
                        <>
                            <Divider style={{backgroundColor: '#E2E2E2', margin: 0}}/>
                            <div className={classes.participantItem}>
                                <span>{e.msisdn}</span>
                                <span>{e.auctionDate}</span>
                            </div>
                        </>
                    )
                })}
                <div className={classes.pagination}>
                    <div className={classes.btnPagination} onClick={() => {
                        if (page > 1) {
                            setLoading(true);
                            fetchListParticipant(page - 1);
                        }
                    }}>
                        <FontAwesomeIcon icon={faChevronLeft}/>
                    </div>
                    <div className={classes.btnPagination}>{page}</div>
                    <div className={classes.btnPagination} onClick={() => {
                        if (player.length == consts.PAGE_SIZE) {
                            setLoading(true);
                            fetchListParticipant(page + 1);
                        }
                    }}>
                        <FontAwesomeIcon icon={faChevronRight}/>
                    </div>

                </div>
            </div> : <div/>}

            {/*
                   list otp modal
            */}


            {otpAddTurn ? <OTPModalV2 handleOk={(otp) => {
                addTurn(otp);
            }}
                                      visible={true}
                                      time={timeOTP}
                                      title={t('otp-modal-title')}
                                      handleCancel={() => {
                                          setOtpAddTurn(false)
                                      }} reSend={sendOtpAddTurn}/> : <div/>}

            {otpRegister ? <OTPModalV2 handleOk={(otp) => {
                registerService(otp);
            }} visible={true} time={timeOTP} title={t('otp-modal-title')} handleCancel={() => {
                setOtpRegister(false)
            }} reSend={sendOtpRegisterService()}/> : <div/>}

            {/* list modal*/}

            <AcceptAuctionModal visible={acceptModal} handleOk={() => {
                setAcceptModal(false);
                playTurn();
            }} phone={isdn()} handleCancel={() => {
                setAcceptModal(false)
            }}/>


            <AddTurnModal visible={addTurnModal} handleOk={() => {
                setAddTurnModal(false);
                setLoading(true);
                sendOtpAddTurn();
            }} phone={isdn()} handleCancel={() => {
                setAddTurnModal(false)
            }}/>


            <RegisterServiceModal visible={registerModal} handleOk={() => {
                setRegisterModal(false)
                setLoading(true);
                sendOtpRegisterService();
            }} phone={isdn()} handleCancel={() => {
                setRegisterModal(false)
            }}/>
            {loading ? <LinearLoading/> : <div/>}
        </div>
    )
}

export default AuctionPage;
