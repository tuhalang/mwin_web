import React, {useEffect, useState} from "react";
import HeaderMobile from "../../components/Header/HeaderMobile";
import {
    faChevronLeft, faChevronRight,
    faUserAlt
} from "@fortawesome/free-solid-svg-icons";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/history/historyPage";
import HistoryItem from "../../components/HistoryItem/HistoryItem";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import consts, {PRIMARY_COLOR} from "../../consts";
import {useTranslation} from "react-i18next";
import utils from "../../common/utils";
import LinearLoading from "../../components/Loading/LinearLoading";

const useStyles = makeStyles(styles);

function HistoryPage(props) {
    const classes = useStyles();
    const [phone, setPhone] = useState(localStorage.getItem('isdn') || '');
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(false);
    const {histories} = props;
    const {t, i18n} = useTranslation('history');

    const onFetchData = (pageNumber = 1, pageSize = consts.PAGE_SIZE) => {
        props.onFetchPaginateHistory({
            'isdn': phone,//phone
            'gameCode': consts.GAME_CODE,
            'language': i18n.language,
            pageNumber,
            pageSize,
        }, (status) => {
            if (!status) {
                //TODO
                //otp error
                return;
            }
            setLoading(false);
            setPage(pageNumber);
        })
    }

    useEffect(() => {
        onFetchData()
    }, [])

    return (
        <>
            <HeaderMobile title={`${t('phone-number-label')}: ${phone}`}
                          icon={false} onBackPage={() => {
            }}/>
            {loading ? <LinearLoading/> :
                <div className={classes.content}>
                    {histories && histories.length > 0 ? _.map(histories, e => <HistoryItem data={e}/>) :
                        <div style={{
                            textAlign: 'center',
                            margin: 20,
                            fontSize: 13,
                            fontWeight: 600
                        }}>{t('no-more-data')}</div>}
                    <div className={classes.pagination}>
                        <div className={classes.btnPagination} onClick={() => {
                            if (page > 1) {
                                setLoading(true);
                                onFetchData(page - 1);
                            }
                        }}>
                            <FontAwesomeIcon icon={faChevronLeft}/>
                        </div>
                        <div className={classes.btnPagination}>{page}</div>
                        <div className={classes.btnPagination} onClick={() => {
                            if (histories.length == consts.PAGE_SIZE) {
                                setLoading(true);
                                onFetchData(page + 1);
                            }
                        }}>
                            <FontAwesomeIcon icon={faChevronRight}/>
                        </div>

                    </div>
                </div>}
        </>
    )
}

export default HistoryPage;
