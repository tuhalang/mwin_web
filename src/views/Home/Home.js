import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/views/components.js";
import HeaderMobile from "../../components/Header/HeaderMobile";
import CardAuction from "../../components/Card/CardAuction";
import Banner from "../../components/Banner/Banner";
import {isMobile} from 'react-device-detect';
import CardAuctionWeek from "../../components/Card/CardAuctionWeek";
import AuctionPage from "../AuctionPage/AuctionPage";
import consts from "../../consts";
import {useTranslation} from "react-i18next";
import _ from 'lodash';
import Loading from "../../components/Loading";
import utils from "../../common/utils";

const useStyles = makeStyles(styles);

export default function Home(props) {
    const classes = useStyles();
    const [subPage, setSubPage] = useState('')
    const {t, i18n} = useTranslation('play');
    const [title, setTitle] = useState('');
    const [dataForward, setDataForward] = useState(null);
    const [loading, setLoading] = useState(false);
    const {images} = props;

    const onFetchData = async (phone = '') => {
        props.onFetchCurrentAuctionSession({
            'isdn': phone,
            gameCode: consts.GAME_CODE,
            language: i18n.language,
        }, () => {
            setLoading(false);
        })
        setTimeout(() => setLoading(false)
            , 2000);
    }

    const checkLogin = () => {
        let token = localStorage.getItem('token');
        if (token && token != '') return true;
        props.openModalLogin()
        return false;
    }

    useEffect(() => {
        setLoading(true)
        onFetchData();
        props.getBanner({
            "isdn": '',
            "gameCode": consts.GAME_CODE,
            "language": i18n.language
        }, () => {

        })
    }, [])

    return (
        <>
            <HeaderMobile title={subPage == '' ? t('title') : title} icon={subPage != '' ? true : false}
                          onBackPage={() => {
                              setSubPage('')
                          }}/>
            {subPage == '' ? <div style={{
                maxHeight: window.innerHeight - 120,
                overflowY: isMobile ? 'scroll' : 'hidden',
            }}>
                {utils.isdn() ? <div style={{
                    textAlign: 'center',
                    width: '100%',
                }}><span style={{
                    fontSize: 13,
                    fontWeight: 600,
                    lineHeight: '18px',
                }}>{t('hello')}: {utils.isdn()}. {t('suggestions-home')}</span>
                </div> : <div/>}
                {loading ? <div style={{justifyContent: 'center', display: 'flex'}}><Loading/></div> :
                    <div className={classes.content}>
                        {
                            _.map(props.sessions || [], e => {
                                if (e.sessionType == 'M') return (
                                    <CardAuction data={e} callback={(data) => {
                                        setDataForward(data);
                                        setTitle(data.sessionName);
                                        setSubPage('auction')
                                    }} key={e.sessionName}/>
                                ); else return (
                                    <CardAuctionWeek data={e} callback={(data) => {
                                        setDataForward(data);
                                        setTitle(data.sessionName);
                                        setSubPage('auction');
                                    }} key={e.sessionName}/>
                                )
                            })
                        }
                        {/*{localStorage.getItem('token') && localStorage.getItem('token') != '' ? <Banner/> : <div/>}*/}
                        {images && images.length > 0 ? <Banner images={images}/> : <div/>}
                    </div>}
            </div> : checkLogin() ? <AuctionPage onPlayTurn={props.onPlayTurn} callback={(idx) => {
                    setSubPage('')
                }} data={dataForward} {...props}/> :
                <div style={{display: 'flex', justifyContent: 'center'}}><Loading/></div>}
        </>
    );
}
