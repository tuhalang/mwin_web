import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons";
import _ from "lodash";
import {Button} from "antd";
import {PRIMARY_COLOR} from "../../consts";

function CancelServiceSuccess(props) {
    return (
        <div style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 16,
        }}>
            <FontAwesomeIcon icon={faCheckCircle} color={'#009E0F'}
                             style={{width: 100, height: 100, marginBottom: 20,}}/>
            <span style={{
                width: 270,
                textAlign: 'center',
                fontSize: 20,
                fontWeight: 500,
                lineHeight: '26px',
                marginBottom: 20
            }}>Chúc mừng bạn đã hủy dịch vụ thành công</span>
            <Button type="primary"
                    style={{
                        backgroundColor: PRIMARY_COLOR,
                        borderRadius: 6,
                        border: 'none',
                        width: '100%',
                        marginTop: 4,
                        '&:hover': {
                            backgroundColor: PRIMARY_COLOR,
                            color: 'white'
                        },
                        '&:focus': {
                            backgroundColor: PRIMARY_COLOR,
                            color: 'white'
                        }
                    }} href={'/play'}>
                {'MÀN HÌNH CHÍNH'}
            </Button>
        </div>
    )
}

export default CancelServiceSuccess;
