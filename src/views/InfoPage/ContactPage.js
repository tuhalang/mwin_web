import React, {useEffect, useState} from "react";
import styles from 'assets/jss/info_user/contactPage'
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ReactComponent as PhoneIcon} from 'assets/img/phone.svg';
import {ReactComponent as GmailIcon} from 'assets/img/gmail.svg';
import {ReactComponent as FacebookIcon} from 'assets/img/facebook.svg';
import consts from "../../consts";
import {useTranslation} from "react-i18next";
import Loading from "../../components/Loading";
import _ from 'lodash'
import {Link} from "react-router-dom";
import {Phone} from "@material-ui/icons";

const useStyles = makeStyles(styles);

function ContactPage(props) {
    const classes = useStyles();
    const {t, i18n} = useTranslation();
    const {getContactInfo, contactUs} = props;
    const [loading, setLoading] = useState(true);

    const onFetchData = () => {
        getContactInfo({
            gameCode: consts.GAME_CODE,
            language: 'en',//i18n.language
        }, () => {
            console.log('get contact info')
            setLoading(false)
        })
    }

    useEffect(() => {
        onFetchData();
    }, [])
    return (
        <div className={classes.container}>
            {
                loading ? <Loading/> : <>
                    <div className={classes.item}><GmailIcon className={classes.icon}/><a
                        // onClick={() => window.open('https://mail.google.com/mail/?view=cm&fs=1&to=' + contactUs ? _.find(contactUs, e => e.name == 'email').value : '', "_blank")}
                        onClick={() => {
                            window.open('https://mail.google.com/mail/?view=cm&fs=1&to=' + (contactUs ? _.find(contactUs, e => e.name == 'email').value : ''), '_blank')
                        }}
                        className={classes.titleItem}>{contactUs ? _.find(contactUs, e => e.name == 'email').value : ''}</a>
                    </div>
                    <div className={classes.item}><PhoneIcon className={classes.icon}/><a
                        className={classes.titleItem}>{contactUs ? _.find(contactUs, e => e.name == 'phone').value : ''}</a>
                    </div>
                    <div className={classes.item}><FacebookIcon className={classes.icon}/><a
                        onClick={() => {
                            let url = _.find(contactUs, e => e.name == 'facebook').value
                            window.open(url.startsWith('https') ? url : 'https://' + url, "_blank")
                        }}
                        className={classes.titleItem}>{_.find(contactUs, e => e.name == 'facebook').value}</a></div>
                </>
            }
        </div>
    )
}

export default ContactPage;
