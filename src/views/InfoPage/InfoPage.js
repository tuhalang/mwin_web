import React, {useState} from "react";
import styles from 'assets/jss/info_user/infoPage'
import {makeStyles} from "@material-ui/core/styles";
import {
    faSignInAlt, faSignOutAlt,
} from "@fortawesome/free-solid-svg-icons";
import HeaderMobile from "../../components/Header/HeaderMobile";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import RuleInfoPage from "./RuleInfoPage";
import {useTranslation} from "react-i18next";
import {PRIMARY_COLOR} from "../../consts";
import _ from 'lodash';
import {Tabs} from "antd";
import mozambique from 'assets/img/mozambique.png';
import uk from 'assets/img/GB.png'

const useStyles = makeStyles(styles);
const {TabPane} = Tabs;

const listLanguage = [

    {
        name: 'en',
        image: uk
    },
    {
        name: 'pt',
        image: mozambique
    },
];

function InfoPage(props) {
    const classes = useStyles();
    const [subPage, setSubPage] = useState('');
    const [tab, setTab] = useState(1);
    const {t, i18n} = useTranslation('info');
    const isLogin = () => localStorage.getItem('token') && localStorage.getItem('token') != '';
    const phone = () => localStorage.getItem('isdn') || '';

    const callback = (activeKey) => {
        setTab(activeKey);
        setSubPage('');
    }


    return (
        <>
            <HeaderMobile title={t('title')} icon={subPage != '' ? true : false} onBackPage={() => {
                setSubPage('')
            }}/>
            <div className={classes.container}>
                <div className={classes.container2}>
                    <div className={classes.container3}>{isLogin() ?
                        <span onClick={() => {
                            props.logout()
                        }}>{phone()} <FontAwesomeIcon icon={faSignOutAlt}/> {t('logout')}</span> :
                        <span onClick={() => {
                            props.login()
                        }}><FontAwesomeIcon icon={faSignInAlt}/> {t('login')}</span>
                    }</div>
                    <div>
                        {_.map(listLanguage, e => <img src={e.image} className={classes.flags} style={{
                            backgroundColor: i18n.language == e.name ? 'white' : PRIMARY_COLOR,
                        }} onClick={() => i18n.changeLanguage(e.name)}/>)}
                    </div>
                </div>
                <Tabs defaultActiveKey="1" onChange={callback} centered tabBarStyle={{
                    color: PRIMARY_COLOR,
                    backgroundColor: 'white',
                }}>
                    <TabPane tab={t('privacy-policy')} key="1">
                        <RuleInfoPage {...props}/>
                    </TabPane>
                    <TabPane tab={t('guide-to-play')} key="2">
                        <RuleInfoPage {...props}/>
                    </TabPane>
                </Tabs>
            </div>
        </>
    )
}

export default InfoPage;
