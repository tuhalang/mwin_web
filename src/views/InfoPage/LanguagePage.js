import React from "react";
import {useTranslation} from "react-i18next";
import _ from 'lodash';
import {Button} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";
import {PRIMARY_COLOR} from "../../consts";


function LanguagePage(props) {

    const {t, i18n} = useTranslation('translation');

    const listLanguage = [

        {
            name: 'en',
            image: 'https://purecatamphetamine.github.io/country-flag-icons/3x2/US.svg'
        },
        {
            name: 'pt',
            image: 'https://purecatamphetamine.github.io/country-flag-icons/3x2/PT.svg'
        },
        {
            name: 'vi',
            image: 'https://purecatamphetamine.github.io/country-flag-icons/3x2/VN.svg'
        },
    ];


    return (
        <div style={{padding: 12, display: 'flex', flexDirection: 'column'}}>
            <h4 style={{color: PRIMARY_COLOR, fontWeight: 400,}}>{t('select-language')}</h4>
            {
                _.map(listLanguage, e => {
                    return (
                        <Button
                            style={{
                                backgroundColor: 'white',
                                height: 50,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                marginBottom: 12,
                            }}
                            onClick={() => {
                                i18n.changeLanguage(e.name)
                            }}
                        >
                            <div style={{display: 'flex', alignItems: 'center'}}>
                                <img src={e.image} style={{
                                    width: 30,
                                    height: 30,
                                    marginRight: 8,
                                    objectFit: 'contain'
                                }}/>
                                <span style={{fontWeight: 400}}>{t(e.name)}</span>
                            </div>
                            {i18n.language.includes(e.name) ?
                                <FontAwesomeIcon icon={faCheck} style={{color: PRIMARY_COLOR}}/> : <div/>}
                        </Button>
                    )
                })
            }
        </div>
    )
}

export default LanguagePage;
