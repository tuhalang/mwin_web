import React, {useState} from "react";
import {Button, Input} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faUserAlt} from "@fortawesome/free-solid-svg-icons";
import OTPInput from "../../components/OTP/OTPInput";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/info_user/personalInfoPage";
import consts, {PRIMARY_COLOR} from "../../consts";
import _ from "lodash";
import {useTranslation} from "react-i18next";
import ModalCancelService from "../../components/Modal/ModalCancelService";
import AcceptAuctionModal from "../../components/Modal/AcceptAuction";
import CancelServiceSuccess from "./CancelServiceSuccess";
import OTPModal from "../../components/OTP/OTPModal";

const useStyles = makeStyles(styles);

function PersonalInfoPage(props) {
    const classes = useStyles();
    const [phone, setPhone] = useState('');
    const [otpCode, setOtpCode] = useState('');
    const [count, setCount] = useState(0);
    const [step, setStep] = useState(0);
    const [cancelModal, setCancelModal] = useState(false);
    const [errorPhone, setErrorPhone] = useState(false);
    const {t, i18n} = useTranslation('history');

    const onSendOTP = (e) => {
        setStep(1);
    }

    return (
        <div {...props} className={classes.container}>
            <div className={classes.container2}>
            {step == 0 ?
                <div className={classes.searchComponent}>
                    <Input prefix={<FontAwesomeIcon icon={faUserAlt} size={16}/>}
                           className={classes.phoneSearch}
                           placeholder={t('phone-placeholder')} style={{marginTop: 12}}
                           onChange={e => setPhone(e.target.value)} value={phone}/>
                    <span className={classes.errorPhone}>{errorPhone ? errorPhone : ''}</span>
                    <Button type="primary"
                            className={classes.btnSearch} onClick={() => {
                        onSendOTP();
                    }}>
                        {t('btn-receive-otp-label')}
                    </Button>
                </div> :
                step == 1 ? <div className={classes.containerStep1}>
                    <span className={classes.titleStep1}>{t('send-otp-label')}</span>
                    <OTPInput
                        autoFocus
                        isNumberInput
                        length={4}
                        className="otpContainer"
                        inputClassName="otpInput"
                        onChangeOTP={(otp) => {
                            if (otp.length == 4) setOtpCode(otp)
                        }}
                        style={{
                            marginBottom: 12,
                        }}
                    />
                    {/*<span style={{fontSize: 12}}>{t('otp-code')}</span>*/}
                    <span style={{
                        fontSize: 13,
                        fontWeight: 400,
                        lineHeight: '17px',
                        marginTop: 20,
                        marginBottom: 20,
                    }}>{t('send-otp-back')} <span style={{color: PRIMARY_COLOR}}>{count}s</span></span>
                    <Button type="primary"
                            className={classes.btnSearch} onClick={() => {
                        //if (otpCode.length == consts.OTP_LENGTH) {
                            setStep(2);
                        //}
                    }}>
                        {t('confirm')}
                    </Button>
                </div> : step == 2 ?
                    <div className={classes.info}>
                        <div className={classes.info2}>
                            <span className={classes.labelInfo}>{t('phone-label')}</span>
                            <span className={classes.inputInfo}>2096091021</span>
                        </div>
                        <div className={classes.info2}>
                            <span className={classes.labelInfo}>{t('name-label')}</span>
                            <span className={classes.inputInfo}>Mr. Diep</span>
                        </div>
                        <div className={classes.info2}>
                            <span className={classes.labelInfo}>VAS CODE</span>
                            <span className={classes.inputInfo}>DGN</span>
                        </div>
                        <Button type={'primary'} className={classes.btnCancel}
                                onClick={() => setCancelModal(true)}>{t('cancel')}</Button>
                    </div> : <CancelServiceSuccess/>}
            </div>
            <OTPModal title={t('service-cancel-label')} visible={cancelModal} handleOk={() => {
                console.log('ok')
                setCancelModal(false);
                setStep(3);
            }} phone={phone} handleCancel={() => {
                setCancelModal(false)
            }}/>
        </div>
    )
}

export default PersonalInfoPage;
