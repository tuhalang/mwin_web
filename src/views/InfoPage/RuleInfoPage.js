import React, {useEffect, useState} from "react";
import styles from 'assets/jss/info_user/rulePage'
import {makeStyles} from "@material-ui/core/styles";
import consts, {PRIMARY_COLOR} from "../../consts";
import Loading from "../../components/Loading";
import {useTranslation} from "react-i18next";
import utils from "../../common/utils";

const useStyles = makeStyles(styles);

const defaultRule = '<div></div>';

function RuleInfoPage(props) {
    const classes = useStyles();
    const {getGuide, rule, title} = props;
    const {t, i18n} = useTranslation('info');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getGuide({
            isdn: utils.isdn() || '',
            gameCode: consts.GAME_CODE,
            language: i18n.language,
        }, () => {
            setLoading(false);
        })
    }, [])

    return (
        <div className={classes.container}>
            {loading ? <Loading/> : <>
                <span style={{color: PRIMARY_COLOR, fontSize: 13, fontWeight: 600,}}>{title}</span>
                <div className={classes.content} dangerouslySetInnerHTML={{__html: `${rule || defaultRule}`}}/>
            </>
            }
        </div>
    )
}

export default RuleInfoPage;
