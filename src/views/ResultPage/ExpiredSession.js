import React, {useEffect, useState} from "react";
import {isMobile, isSafari} from "react-device-detect";
import Loading from "../../components/Loading";
import _ from "lodash";
import styles from 'assets/jss/result/onGoingSession'
import {makeStyles} from "@material-ui/core/styles";
import consts from "../../consts";
import CardResult from "../../components/Card/CardResult";
import CardResultWeek from "../../components/Card/CardResultWeek";
import ResultOnGoingDetail from "./ResultOnGoingDetail";
import ResultExpiredDetail from "./ResultExpiredDetail";

const useStyles = makeStyles(styles);

function ExpiredSession(props) {
    const classes = useStyles();
    const {subPage, callback} = props;
    const [loading, setLoading] = useState(false);
    const [dataForward, setDataForward] = useState(null);
    const [errorMessage, setErrorMessage] = useState('');

    const onFetchData = async (phone = '') => {
        props.onFetchExpiredAuctionSessionResult({
            // props.onFetchCurrentAuctionSessionResult({
            'isdn': phone,
            gameCode: consts.GAME_CODE,
            // language: i18n.language,
            language: 'en'
        }, (message) => {
            setLoading(false);
            console.log('fetch data current auction session')
            setErrorMessage(message);
        })
        setTimeout(() => setLoading(false)
            , 2000);
    }

    useEffect(() => {
        if (props.tab == 2) {
            setLoading(true)
            onFetchData(localStorage.getItem('isdn') || '');
        }
    }, [props.tab])

    return (
        <div>
            {subPage == '' ? <div style={{
                flex: 1,
            }}>
                {loading ? <div style={{justifyContent: 'center', display: 'flex'}}><Loading/></div> :
                    <div className={classes.content}>
                        {
                            props.sessions ? _.map(props.sessions || [], e => {
                                if (e.sessionType == 'M') return (
                                    <CardResult data={e} callback={(data) => {
                                        setDataForward(data);
                                        callback('result_detail')
                                    }} isRunning={false} key={e.sessionName}/>
                                ); else return (
                                    <CardResultWeek data={e} callback={(data) => {
                                        setDataForward(data)
                                        callback('result_detail')
                                    }} isRunning={false} key={e.sessionName}
                                    />
                                )
                            }) : <span style={{textAlign: 'center'}}>{errorMessage}</span>
                        }
                    </div>}
            </div> : subPage == 'result_detail' && dataForward ?
                <ResultExpiredDetail {...props} data={dataForward} expired={true} callback={() => callback('')}/> :
                <div/>}
        </div>
    )
}


export default ExpiredSession;
