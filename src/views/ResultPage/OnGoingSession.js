import React, {useEffect, useState} from "react";
import {isMobile} from "react-device-detect";
import Loading from "../../components/Loading";
import _ from "lodash";
import styles from 'assets/jss/result/onGoingSession'
import {makeStyles} from "@material-ui/core/styles";
import consts from "../../consts";
import CardResult from "../../components/Card/CardResult";
import CardResultWeek from "../../components/Card/CardResultWeek";
import ResultOnGoingDetail from "./ResultOnGoingDetail";
import {useTranslation} from "react-i18next";
import {isSafari} from 'react-device-detect';

const useStyles = makeStyles(styles);

function OnGoingSession(props) {
    const classes = useStyles();
    const {subPage, callback} = props;
    const [loading, setLoading] = useState(false);
    const [dataForward, setDataForward] = useState(null);
    const {t, i18n} = useTranslation('result');

    const onFetchData = (phone = '') => {
        props.onFetchCurrentAuctionSessionResult({
            'isdn': phone,
            gameCode: consts.GAME_CODE,
            language: i18n.language,
        }, () => {
            setLoading(false);
            console.log('fetch data current auction session')
        })
        setTimeout(() => setLoading(false)
            , 2000);
    }

    console.log(props.sessions, "sessions")

    useEffect(() => {
        if (props.tab == 1) {
            setLoading(true)
            onFetchData(localStorage.getItem('isdn') || '');
            console.log('abc');
        }
    }, [props.tab])

    return (
        <div>
            {subPage == '' ? <div style={{
                flex: 1,
                marginBottom: 65,
                overflowY: isSafari ? 'scroll' : 'auto',
                "-webkit-overflow-scrolling": 'touch'
            }}>
                {loading ? <div style={{justifyContent: 'center', display: 'flex'}}><Loading/></div> :
                    <div className={classes.content}>
                        {
                            _.map(props.sessions || [], e => {
                                if (e.sessionType == 'M') return (
                                    <CardResult data={e} callback={(data) => {
                                        setDataForward(data);
                                        callback('result_detail');
                                    }} key={e.sessionName}/>
                                ); else return (
                                    <CardResultWeek data={e} callback={(data) => {
                                        setDataForward(data);
                                        callback('result_detail');
                                    }} key={e.sessionName}/>
                                )
                            })
                        }
                    </div>}
            </div> : subPage == 'result_detail' && dataForward ?
                <ResultOnGoingDetail {...props} data={dataForward} callback={() => callback('')}/> : <div/>}
        </div>
    )
}

export default OnGoingSession;
