import React, {useEffect, useState} from "react";
import consts from "../../consts";
import styles from 'assets/jss/result/resultDetail'
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import _ from 'lodash';
import {useTranslation} from "react-i18next";
import ResultItem from "./ResultItem";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {Tabs} from "antd";
import LinearLoading from "../../components/Loading/LinearLoading";

const useStyles = makeStyles(styles);

const {TabPane} = Tabs;

function ResultExpiredDetail(props) {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const {t, i18n} = useTranslation('result');
    const {result, data} = props;
    const [winner, setWinner] = useState(null);

    console.log(data, 'dataForward');

    const onFetchData = (pageNumber = 1, pageSize = 6) => {
        props.onFetchListWinnerPrize({
            gameCode: consts.GAME_CODE,
            language: i18n.language,
            auctionSessionId: data.auctionSessionId,
            productId: data.productId,
            pageNumber,
            pageSize,
        }, (data) => {
            setLoading(false);
            setPage(pageNumber);
            console.log('fetch data result detail', data)
        })
        setTimeout(() => setLoading(false)
            , 2000);
    }

    const callback = (key) => {
        console.log(key);
    }
    const renderTabBar = (props, DefaultTabBar) => {
        console.log(props);
        return (
            <div className={classes.tabBar}>
                <span className={props.activeKey == '1' ? classes.tabActive : classes.tab}
                      onClick={() => props.onTabClick('1')}>{t('winner')}</span>
                <span className={props.activeKey == '2' ? classes.tabActive : classes.tab}
                      onClick={() => props.onTabClick('2')}>{t('daily')}</span>
            </div>
        )
    }

    useEffect(() => {
        setLoading(true)
        onFetchData();
    }, [])

    useEffect(() => {
        if (result && page == 1) setWinner(_.head(result))
    }, [props.result]);


    return (
        <div className={classes.container}>
            <div className={classes.header}>
                <FontAwesomeIcon icon={faChevronLeft} style={{width: 15, height: 15}} size={10}
                                 onClick={() => props.callback()}/>
                <span>{_.upperCase(data ? data.sessionName : '')}</span>
                <div style={{width: 15, height: 15}}></div>
            </div>
            <Tabs defaultActiveKey="1" onChange={callback} renderTabBar={renderTabBar}>
                <TabPane tab="Tab 1" key="1">
                    {winner ? <ResultItem data={winner} imageUrl={data ? data.imageUrl : ''}/> : <div/>}
                </TabPane>
                <TabPane tab="Tab 2" key="2">
                    {_.map(result, e => {
                        return <ResultItem data={e} imageUrl={data ? data.imageUrl : ''} key={e.id}/>
                    })}
                    <div className={classes.pagination}>
                        <div className={classes.btnPagination} onClick={() => {
                            if (page > 1) {
                                setLoading(true);
                                onFetchData(page - 1);
                            }
                        }}>
                            <FontAwesomeIcon icon={faChevronLeft}/>
                        </div>
                        <div className={classes.btnPagination}>{page}</div>
                        <div className={classes.btnPagination} onClick={() => {
                            if (result.length == consts.PAGE_SIZE) {
                                setLoading(true);
                                onFetchData(page + 1);
                            }
                        }}>
                            <FontAwesomeIcon icon={faChevronRight}/>
                        </div>

                    </div>
                </TabPane>
            </Tabs>
            {loading ? <LinearLoading/> : <div/>}
        </div>
    )
}

export default ResultExpiredDetail;
