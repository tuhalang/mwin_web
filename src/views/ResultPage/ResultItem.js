import React from "react";
import styles from 'assets/jss/result/resultItem';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import {useTranslation} from "react-i18next";
import utils from "../../common/utils";


const useStyles = makeStyles(styles);

function ResultItem(props) {
    const classes = useStyles();
    const {t} = useTranslation('result');
    const {data, imageUrl} = props;

    return (
        <div className={classes.container}>
            <img src={utils.getImageUrl(imageUrl || '')} className={classes.image}/>
            <div className={classes.content}>
                <span className={classes.productName}>{t('product-label')}: {data.productName}</span>
                <span>{t('phone-number-label')}: {data.msisdn}</span>
                <span><FontAwesomeIcon icon={faClock}/> {data.prizeDate}</span>
            </div>
            <span className={classes.price}>3,000,00$</span>
        </div>
    )
}

export default ResultItem;
