import React, {useEffect, useState} from "react";
import consts from "../../consts";
import styles from 'assets/jss/result/resultDetail'
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import _ from 'lodash';
import {useTranslation} from "react-i18next";
import ResultItem from "./ResultItem";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles(styles);

function ResultOnGoingDetail(props) {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const {t, i18n} = useTranslation('result');
    const {result, data} = props;

    const onFetchData = (pageNumber = 1, pageSize = 6) => {
        props.onFetchListWinnerPrize({
            gameCode: consts.GAME_CODE,
            language: i18n.language,
            auctionSessionId: data.auctionSessionId,
            productId: data.productId,
            pageNumber,
            pageSize,
        }, (data) => {
            setLoading(false);
            setPage(pageNumber);
            console.log('fetch data result detail', data)
        })
        setTimeout(() => setLoading(false)
            , 2000);
    }

    console.log(props.sessions, "sessions")

    useEffect(() => {
        if (props.data) {
            setLoading(true)
            onFetchData();
        }
    }, [])

    return (
        <div className={classes.container}>
            <div className={classes.header}>
                <FontAwesomeIcon icon={faChevronLeft} style={{width: 15, height: 15}} size={10}
                                 onClick={() => props.callback()}/>
                <span>{_.upperCase(data ? data.sessionName : '')}</span>
                <div style={{width: 15, height: 15}}></div>
            </div>
            {_.map(result, e => {
                return <ResultItem data={e} imageUrl={data ? data.imageUrl : ""} key={e.id}/>
            })}
            <div className={classes.pagination}>
                <div className={classes.btnPagination} onClick={() => {
                    if (page > 1) {
                        setLoading(true);
                        onFetchData(page - 1);
                    }
                }}>
                    <FontAwesomeIcon icon={faChevronLeft}/>
                </div>
                <div className={classes.btnPagination}>{page}</div>
                <div className={classes.btnPagination} onClick={() => {
                    // if (histories.length == consts.PAGE_SIZE) {
                    //     setLoading(true);
                    //     onFetchData(page + 1);
                    // }
                }}>
                    <FontAwesomeIcon icon={faChevronRight}/>
                </div>

            </div>
        </div>
    )
}

export default ResultOnGoingDetail;
