import React, {useState} from "react";
import styles from 'assets/jss/result';
import {makeStyles} from "@material-ui/core/styles";
import {Tabs} from "antd";
import {faChevronLeft, faGavel} from "@fortawesome/free-solid-svg-icons";
import HeaderMobile from "../../components/Header/HeaderMobile";
import {PRIMARY_COLOR} from "../../consts";
import OnGoingSession from "./OnGoingSession";
import ExpiredSession from "./ExpiredSession";
import Navigation from "../../components/Navigation/Navigation";
import {useTranslation} from "react-i18next";

const {TabPane} = Tabs;

const useStyles = makeStyles(styles);

function ResultPage(props) {
    const classes = useStyles();
    const [subPage, setSubPage] = useState('');
    const [tab, setTab] = useState(1);
    const {t, i18n} = useTranslation('result');

    const callback = (activeKey) => {
        setTab(activeKey);
        setSubPage('');
    }

    return (
        <div className={classes.container2}>
            <HeaderMobile title={t('title')} icon={subPage != '' ? true : false} onBackPage={() => {
                setSubPage('')
            }}/>
            <Tabs defaultActiveKey="1" onChange={callback} centered tabBarStyle={{
                color: PRIMARY_COLOR,
                backgroundColor: 'white',
            }}>
                <TabPane tab={t("on-going-session")} key="1">
                    <OnGoingSession {...props} subPage={subPage} callback={(subPage) => {
                        setSubPage(subPage)
                    }} tab={tab}/>
                </TabPane>
                <TabPane tab={t("expired-session")} key="2">
                    <ExpiredSession {...props} subPage={subPage} callback={(subPage) => {
                        setSubPage(subPage)
                    }} tab={tab}/>
                </TabPane>
            </Tabs>
        </div>
    )
}

export default ResultPage;
